<?php

namespace App\Console\Commands;

use App\Models\Car;
use App\Enums\User\Destination;
use Illuminate\Console\Command;
use Spatie\Sitemap\SitemapGenerator;
use Spatie\Sitemap\Tags\Url;

class SeoGenerateSiteMap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Website Sitemap';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        SitemapGenerator::create(config('app.url'))
                        ->getSitemap()
                        ->add(Car::all())
                        ->add(
                            Url::create(route('app.destinations', Destination::NORTH))
                               ->setPriority(0.8)
                        )
                        ->add(
                            Url::create(route('app.destinations', Destination::EAST))
                               ->setPriority(0.8)
                        )
                        ->add(
                            Url::create(route('app.destinations', Destination::WEST))
                               ->setPriority(0.8)
                        )
                        ->add(
                            Url::create(route('app.destinations', Destination::SOUTH))
                               ->setPriority(0.8)
                        )
                        ->add(
                            Url::create(route('tour.booking'))
                               ->setPriority(0.5)
                        )
                        ->add(
                            Url::create(route('taxi.booking'))
                               ->setPriority(0.5)
                        )
                        ->writeToFile(public_path('sitemap.xml'));

        return 0;
    }
}
