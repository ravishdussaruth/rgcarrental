<?php

namespace App\Http\Controllers;

use App\Models\RentDiscount;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class DiscountController extends Controller
{
    /**
     * Retrieve the discount.
     *
     * @param  Request  $request
     * @return JsonResponse
     */
    public function __invoke(Request $request): JsonResponse
    {
        $request->validate([
            'days' => 'required|min:0',
        ]);

        /**
         * @var RentDiscount $rentDiscount
         */
        $rentDiscount = \App\Queries\RentDiscount::execute($request->days)->first();

        return response()->json([
            'is_valid' => !is_null($rentDiscount),
            'discount_id' => $rentDiscount?->discount->uuid,
            'value' => $rentDiscount?->discount->coeff,
        ]);
    }
}
