<lit-col width="12" class="d-flex justify-content-center">
    <div>
        <div class="text-center d-flex flex-column mb-2">
            @include('litstack::partials.logo')
            <span class="mt-2" style="font-size:2.5rem;">{!! __lit('base.user_hello', ['user' => '<strong>'.lit_user()->first_name.'</strong>']) !!}.</span>
        </div>
    </div>
</lit-col>
