<?php

namespace App\Contracts\Booking;

use App\Models\Booking;

interface Notification
{
    /**
     * Notify owner about booking.
     *
     * @param Booking $booking
     *
     * @return void
     */
    public function notifyOwnerAboutBooking(Booking $booking);

    /**
     * Notify user booking has been created.
     *
     * @param Booking $booking
     *
     * @return void
     */
    public function notifyCreated(Booking $booking);

    /**
     * Notify user about booking has been confirmed.
     *
     * @param Booking $booking
     *
     * @return void
     */
    public function notifyConfirmed(Booking $booking);

    /**
     * Notify user about booking has been canceled.
     *
     * @param Booking $booking
     *
     * @return void
     */
    public function notifyCanceled(Booking $booking);

    /**
     * Notify user about booking have been completed.
     *
     * @param Booking $booking
     *
     * @return void
     */
    public function notifyCompleted(Booking $booking);
}
