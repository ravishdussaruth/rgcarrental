<?php

namespace App\Http\Controllers\Page;

use App\Models\Car;
use App\Models\Service;
use App\Contracts\Home;
use App\Models\Destination;
use App\Http\Controllers\Controller;
use App\Enums\User\Destination as Destinations;

class Index2Controller extends Controller
{
    /**
     * Retrieve home details.
     *
     * @param Home $home
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function __invoke(Home $home)
    {
        return view('index2', [
            'cover'        => $home->cover(),
            'cars'         => Car::all()->toJson(),
            'services'     => Service::all()->toJson(),
            'regions'      => Destinations::toJson(),
            'destinations' => Destination::whereDisplayOnSite(true)->get()->toJson()
        ]);
    }
}
