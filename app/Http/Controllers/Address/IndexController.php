<?php

namespace App\Http\Controllers\Address;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\Location as Query;

class IndexController extends Controller
{
    /**
     * Search all address.
     *
     * @param  Request  $request
     * @param  Query  $location
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request, Query $location)
    {
        return collect(
            $location->q($request->query->get('query'))
                     ->get()
                     ->json()
        )->unique('display_name')
         ->pluck('display_name');
    }
}
