<?php

namespace Lit\Config\Crud\Concerns;

use App\Enums\Currency;
use App\Models\Booking;
use App\Models\Discount;
use App\Models\User;
use Ignite\Crud\Config\CrudConfig;
use Ignite\Crud\CrudIndex;
use Ignite\Crud\CrudIndexTable;
use Ignite\Crud\CrudShow;
use Lit\Actions\AddBookingPaymentAction;
use Lit\Actions\RemoveBookingPaymentAction;
use Lit\Config\Charts\Booking\RentalBookingNumberChartConfig;

class BaseBookingConfig extends CrudConfig
{
    /**
     * Model class.
     *
     * @var string
     */
    public $model = Booking::class;

    /**
     * Build index page.
     *
     * @param \Ignite\Crud\CrudIndex $page
     *
     * @return void
     */
    public function index(CrudIndex $page)
    {
        if ($this->hasBookingPayment()) {
            $page->chart(RentalBookingNumberChartConfig::class)
                 ->width(2 / 5)
                 ->height('120px')
                 ->variant('primary');
        }

        $this->table($page)
             ->search([
                 'reference',
                 'user.first_name'
             ])->filter([
                'Booking' => [
                    'Today'              => 'Today',
                    'Completed'          => 'Completed',
                    'Pending'            => 'Pending',
                    'WaitingForApproval' => 'Waiting For Approval',
                    'Approved'           => 'Approved'
                ]
            ]);
    }

    /**
     * Setup show page.
     *
     * @param \Ignite\Crud\CrudShow $page
     *
     * @return void
     */
    public function show(CrudShow $page)
    {
        $page->group(function ($page) {
            $page->onlyOnUpdate(function (CrudShow $page) {
                $page->card(function ($form) {
                    if (!$this->hasBookingPayment()) {
                        $form->select('currency')
                             ->title('Currency')
                             ->options(Currency::labels()->toArray())
                             ->width(1 / 3);

                        $form->input('price')
                             ->title('Price')
                             ->width(1 / 3);

                        $form->select('discount_id')
                             ->title('Discount')
                             ->options(
                                 Discount::all()->mapWithKeys(fn(Discount $discount) => [$discount->id => $discount->name])->toArray()
                             )
                             ->width(1 / 3);
                    } else {
                        $form->info('Currency')
                             ->text(Currency::labels()->get($this->modelInstance->currency))
                             ->width(1 / 3);

                        $form->info('Price')
                             ->text($this->modelInstance->price)
                             ->width(1 / 3);

                        $form->info('Discount')
                             ->text($this->modelInstance->discount->name ?? 'N/A')
                             ->width(1 / 3);
                    }
                })->title('Finance');
            });

            $this->baseForm($page);
        })->width(7);

        $page->group(function ($page) {
            $this->rightSideView($page);
            $this->extraSideView($page);
        })->width(5);
    }

    /**
     * Items in side view.
     *
     * @param CrudShow $page
     *
     * @return void
     */
    protected function extraSideView(CrudShow $page): void
    {
        // Add extra views.
    }

    /**
     * Items in side view.
     *
     * @param CrudShow $page
     *
     * @return void
     */
    protected function rightSideView(CrudShow $page): void
    {
        $this->baseSide($page);
        $this->side($page);
    }

    /**
     * Display item in the side view.
     *
     * @param CrudShow $page
     *
     * @return void
     */
    protected function side(CrudShow $page): void
    {
        $page->onlyOnUpdate(function ($page) {
            $page->card(function ($form) {
                $form->info('Remarks')
                     ->text($this->ifNotAvailable(optional($this->modelInstance)->remarks))
                     ->width(1 / 2);

                $form->info('Budget')
                     ->text($this->ifNotAvailable(optional($this->modelInstance)->budget))
                     ->width(1 / 2);
            })->title('Other Details');
        });
    }

    /**
     * Base item to show in the side view.
     *
     * @param CrudShow $page
     *
     * @return void
     */
    protected function baseSide(CrudShow $page): void
    {
        $page->onlyOnUpdate(function ($page) {
            $page->card(function ($form) {
                $form->input('original_price')
                     ->type('number');
            })->title('Original Price');

            $page->card(function ($form) {
                $form->info('First Name')
                     ->text('{user.first_name}')
                     ->width(1 / 2);

                $form->info('Last Name')
                     ->text('{user.last_name}')
                     ->width(1 / 2);

                $form->info('Phone Number')
                     ->text('{user.phone_number}');

                $form->info('Email')
                     ->text('{user.email}');
            })->title('User Info');
        });

        if (isset($this->modelInstance->original_price)) {
            $page->onlyOnUpdate(function ($page) {
                if (!$this->hasBookingPayment()) {
                    $page->navigationRight()
                         ->action('Payment', AddBookingPaymentAction::class)
                         ->variant('primary');
                }

                if ($this->hasBookingPayment()) {
                    $page->navigationControls()
                         ->action('Remove Payment', RemoveBookingPaymentAction::class);

                    $page->card(function ($page) {
                        $page->info('Payment Type')
                             ->text($this->modelInstance->bookingPayment->paymentType->name)
                             ->width(1 / 2);

                        if ($this->modelInstance->bookingPayment->completed) {
                            $page->info('Completed')
                                 ->text('<div class="badge badge-success">Yes</div>')
                                 ->width(1 / 2);
                        }

                        if (!$this->modelInstance->bookingPayment->completed) {
                            $page->info('Completed')
                                 ->text('<div class="badge badge-danger">No</div>')
                                 ->width(1 / 2);
                        }

                    })
                         ->title('Payment: ' . $this->modelInstance->bookingPayment->created_at->format('Y-m-d H:i'));
                }
            });
        }
    }

    /**
     * The booking table.
     *
     * @param CrudIndex $page
     *
     * @return CrudIndexTable
     */
    protected function table(CrudIndex $page): CrudIndexTable
    {
        return $page->table(fn() => []);
    }

    /**
     * The base form.
     *
     * @param CrudShow $page
     */
    protected function baseForm(CrudShow $page)
    {
        $page->card(function ($page) {
            $page->onlyOnUpdate(function ($form) {
                $form->boolean('approved')
                     ->title('Approve Booking');
            });

            $page->input('flight_number')
                 ->title('Flight Number')
                 ->width(1 / 2);

            $page->select('user_id')
                 ->title('User')
                 ->options(
                     User::all()->mapWithKeys(fn($item) => [$item->id => $item->name])->toArray()
                 )->hint('Select a user')
                 ->width(1 / 2);

            $this->form($page);
        })->title('Booking Info');
    }

    /**
     * The booking form.
     *
     * @param CrudShow $page
     *
     * @return void
     */
    protected function form(CrudShow $page)
    {
        // TODO: Insert form code here.
    }


    /**
     * Will tell us if booking payment is available.
     *
     * @return bool
     */
    protected function hasBookingPayment(): bool
    {
        return !is_null($this->modelInstance) && $this->modelInstance->bookingPayment()->exists();
    }

    /**
     * Not available value.
     *
     * @param mixed $value
     *
     * @return string
     */
    protected function ifNotAvailable($value): string
    {
        return $value ?? 'N/A';
    }
}
