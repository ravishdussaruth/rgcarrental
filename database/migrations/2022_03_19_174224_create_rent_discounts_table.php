<?php

use App\Support\Migrationable;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRentDiscountsTable extends Migration
{
    use Migrationable;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rent_discounts', function (Blueprint $table) {
            $table->id();
            $table->integer('min_day')->default(1);
            $table->integer('max_day')->default(1);
            $table->integer('diff_days')->virtualAs('max_day - min_day')->nullable();
            $table->unsignedBigInteger('discount_id');
            $table->timestamps();
        });

        if ($this->isNotTesting()) {
            Schema::table('rent_discounts', function (Blueprint $table) {
                $table->foreign('discount_id')
                      ->references('id')
                      ->on('discounts')
                      ->cascadeOnDelete();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ($this->isNotTesting()) {
            Schema::table('rent_discounts', function (Blueprint $table) {
                $table->dropForeign(['discount_id']);
            });
        }

        Schema::dropIfExists('rent_discounts');
    }
}
