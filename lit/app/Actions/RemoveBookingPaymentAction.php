<?php

namespace Lit\Actions;

use App\Models\Booking;
use Ignite\Support\AttributeBag;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;
use Ignite\Page\Actions\ActionModal;

class RemoveBookingPaymentAction
{
    /**
     * Display the modal.
     *
     * @param ActionModal $modal
     */
    public function modal(ActionModal $modal)
    {
        $modal->confirmVariant('danger')
              ->confirmText('Remove Payment')
              ->message('Do you want to remove this payment?');;
    }

    /**
     * Run the action.
     *
     * @param Collection   $models
     * @param AttributeBag $attributes
     *
     * @return JsonResponse
     */
    public function run(Collection $models, AttributeBag $attributes)
    {
        /** @var Booking $model */
        $model = $models->first();

        $model->bookingPayment()
              ->delete();

        return redirect($model->route());
    }
}
