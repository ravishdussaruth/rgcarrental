<?php

namespace App\Models\Concerns;

use App\Enums\BookingType;

trait HasBookingType
{
    /**
     * Will tell us if this booking is for car rent.
     *
     * @return bool
     */
    public function isRentType(): bool
    {
        return $this->booking_type === BookingType::RENT;
    }

    /**
     * Will tell us if this booking is for tour.
     *
     * @return bool
     */
    public function isTourType(): bool
    {
        return $this->booking_type === BookingType::TOUR;
    }

    /**
     * Will tell us if this booking is for taxi.
     *
     * @return bool
     */
    public function isTaxiType(): bool
    {
        return $this->booking_type === BookingType::TAXI;
    }
}
