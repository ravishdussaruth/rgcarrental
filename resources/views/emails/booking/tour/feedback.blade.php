@extends('emails._partial')

@section('content')
    <div class="w-50">
        <p>Hello {{ $user->first_name }}</p>
        <p>Thanks for using RgCarRental for your tour booking. Please tell us about your experience by clicking the
            button
            below. Your feedback helps us create a better experience for you and for all our customers</p>

        <a href="{{ $feedbackUrl }}" class="btn btn-primary">Tell us how it went</a>
    </div>
@endsection
