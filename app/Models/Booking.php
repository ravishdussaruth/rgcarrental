<?php

namespace App\Models;

use App\Enums\BookingType;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Carbon;
use App\Enums\User\Destination;
use App\Models\Concerns\HasDiscount;
use Illuminate\Database\Eloquent\Model;
use App\Models\Concerns\HasBookingType;
use App\Models\Concerns\HasNotifications;
use App\Models\Concerns\HasBookingUpdated;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Booking extends Model
{
    use SoftDeletes, HasFactory, HasNotifications, HasBookingUpdated, HasDiscount, HasBookingType;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'car_id',
        'pickup_id',
        'drop_id',
        'pickup_time',
        'drop_time',
        'delivered_on',
        'returned_on',
        'approved',
        'reference',
        'rejected_comment',
        'booking_type',
        'pickup_address',
        'drop_address',
        'no_of_passenger',
        'discount_id',
        'price',
        'budget',
        'remarks',
        'currency',
        'original_price',
        'regions',
        'flight_number',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'approved' => 'boolean',
        'regions' => 'array',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'user',
    ];

    /**
     * Get all booking for today.
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeToday($query)
    {
        return $query->whereDate('pickup_time', Carbon::today());
    }

    /**
     * Retrieve all booking that have been completed.
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeCompleted($query)
    {
        return $query->whereNotNull('returned_on');
    }

    /**
     * Retrieve all booking that are pending.
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopePending($query)
    {
        return $query->whereNull('returned_on');
    }

    /**
     * Booking approved.
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeApproved($query)
    {
        return $query->where('approved', true);
    }

    /**
     * Booking waiting to be approved.
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeWaitingForApproval($query)
    {
        return $query->where('approved', false);
    }

    /**
     * Booking regions by the user.
     *
     * @return string
     */
    public function getBookingRegionsAttribute()
    {
        $regions = collect($this->regions);

        return Destination::all()->filter(fn($bookingType) => $regions->contains($bookingType['id']))
                          ->pluck('label')
                          ->join(', ');
    }

    /**
     * The booking route.
     *
     * @return string
     */
    public function route(): string
    {
        return route(BookingType::routes()->get($this->booking_type), $this);
    }

    /**
     * The discount for this booking.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function discount()
    {
        return $this->belongsTo(Discount::class);
    }

    /**
     * The car associated to this booking.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function car()
    {
        return $this->belongsTo(Car::class);
    }

    /**
     * The user associated to this booking.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * The drop location.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function dropLocation()
    {
        return $this->belongsTo(Location::class, 'drop_id');
    }

    /**
     * The pickup location.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pickupLocation()
    {
        return $this->belongsTo(Location::class, 'pickup_id');
    }

    /**
     * The feedbacks for this booking.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function feedbacks()
    {
        return $this->hasMany(Feedback::class);
    }

    /**
     * The payment details for this booking.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function bookingPayment()
    {
        return $this->hasOne(BookingPayment::class);
    }
}
