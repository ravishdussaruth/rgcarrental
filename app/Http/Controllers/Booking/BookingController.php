<?php

namespace App\Http\Controllers\Booking;

use App\Models\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Booking\BookingRequest;

class BookingController extends Controller
{
    /**
     * New car booking.
     *
     * @param  BookingRequest  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(BookingRequest $request)
    {
        // Create User.
        $user = $this->createOrFindTheUser($request->user);

        $booking = $this->book(
            $user,
            array_merge(
                $request->booking(),
                [
                    'reference' => generate_booking_id(),
                ]
            ),
            $request->isRentType()
        );

        return response()->json([
            'booked' => !is_null($booking),
            'reference' => $booking->reference,
        ]);
    }

    /**
     * Create a user booking.
     *
     * @param  User  $user
     * @param  array  $booking
     * @param  bool  $carRental
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    protected function book(User $user, array $booking, bool $carRental)
    {
        if ($carRental) {
            $booking = array_merge($booking, ['original_price' => $booking['price'] ?? 0]);
        }

        return $user->bookings()
                    ->create($booking);
    }

    /**
     * Create a user if not exists or search for existing user.
     *
     * @param  array  $detail
     *
     * @return User
     */
    protected function createOrFindTheUser(array $detail): User
    {
        return User::firstOrCreate(
            array_merge(
                [
                    'email' => $detail['email'],
                    'phone_number' => $detail['phone_number'],
                ],
                $detail,
            )
        );
    }
}
