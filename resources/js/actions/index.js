import { book } from '@/api/booking';

/**
 * Rent a car.
 *
 * @param {Object} data
 *
 * @return {Promise<*>}
 */
export const rent = async(data) => await book({...data, ... { booking_type: 3 } });

/**
 * Book a tour.
 *
 * @param {Object} data
 *
 * @return {Promise<*>}
 */
export const tour = async(data) => await book({...data, ... { booking_type: 1 } });

/**
 * Book a taxi.
 *
 * @param {Object} data
 *
 * @return {Promise<*>}
 */
export const taxi = async(data) => await book({...data, ... { booking_type: 2 } });