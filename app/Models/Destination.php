<?php

namespace App\Models;

use Ignite\Crud\Models\Traits\HasMedia;
use App\Enums\User\Destination as Enum;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Rennokki\QueryCache\Traits\QueryCacheable;
use Spatie\MediaLibrary\HasMedia as HasMediaContract;

class Destination extends Model implements HasMediaContract
{
    use HasMedia, QueryCacheable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'category', 'display_on_site'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'display_on_site' => 'boolean',
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['media'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['image', 'destination_category'];

    /**
     * View only destinations for north.
     *
     * @param  Builder  $query
     *
     * @return Builder
     */
    public function scopeNorth(Builder $query)
    {
        return $query->where('category', Enum::NORTH);
    }

    /**
     * View only destinations for east.
     *
     * @param  Builder  $query
     *
     * @return Builder
     */
    public function scopeEast(Builder $query)
    {
        return $query->where('category', Enum::EAST);
    }

    /**
     * View only destinations for west.
     *
     * @param  Builder  $query
     *
     * @return Builder
     */
    public function scopeWest(Builder $query)
    {
        return $query->where('category', Enum::WEST);
    }

    /**
     * View only destinations for south.
     *
     * @param  Builder  $query
     *
     * @return Builder
     */
    public function scopeSouth(Builder $query)
    {
        return $query->where('category', Enum::SOUTH);
    }

    /**
     * Image attribute.
     *
     * @return \Lit\Crud\Models\Media
     */
    public function getImageAttribute()
    {
        return $this->getMedia('image')->first();
    }

    /**
     * Retrieve destination category text.
     *
     * @return string
     */
    public function getDestinationCategoryAttribute()
    {
        return Enum::getDescription($this->category);
    }
}
