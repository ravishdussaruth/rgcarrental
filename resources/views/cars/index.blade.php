@extends('partials.app', ['position' => 'position:relative'])

@section('app')
	<div class="pt-lg-5 mt-lg-5 p-2 container">
		<div class="d-flex flex-column justify-content-center align-items-center">
			<search-car class="bg-white p-1"
						start="{{ request()->pickup_time }}"
						end="{{ request()->drop_time }}"
						selected-transmission="{{ request()->transmission }}"
						inline>
				<template #header>
					<span class="text-left text-dark w-100 mt-2 opacity-50">Search and Filtering</span>
				</template>
			</search-car>

			<div class="container overflow-scroll">
				<h4 class="text-center mt-2">Select a car from our collection</h4>
				<hr/>

				<list :params="{{ json_encode(request()->all()) }}" :cars="{{ $cars->toJson() }}"></list>
			</div>
		</div>
	</div>
@endsection
