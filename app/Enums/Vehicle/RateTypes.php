<?php


namespace App\Enums\Vehicle;

class RateTypes extends \App\Enums\Enum
{
    /**
     * @var int
     */
    const PER_DAY = 1;

    /**
     * @var int
     */
    const PER_WEEK = 2;

    /**
     * @var int
     */
    const PER_MONTH = 3;
}
