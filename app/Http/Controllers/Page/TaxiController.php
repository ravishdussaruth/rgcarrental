<?php

namespace App\Http\Controllers\Page;

use App\Contracts\Home;
use App\Http\Controllers\Concerns\HasSeo;
use App\Http\Controllers\Controller;
use Artesaos\SEOTools\Facades\SEOMeta;
use Facades\Artesaos\SEOTools\SEOTools;
use Illuminate\Http\Request;

class TaxiController extends Controller
{
    use HasSeo;

    public function __invoke(Home $home)
    {
        $this->render($home);

        SEOMeta::setDescription('Book a taxi online with us. Available 24/7 anywhere in Mauritius.');

        return view('taxi');
    }
}
