<?php

namespace Lit\Http\Controllers\Crud;

use App\Enums\BookingType;
use Lit\Http\Controllers\Crud\Concerns\BaseBookingController;

class TaxiBookingController extends BaseBookingController
{
    /**
     * Assign the booking type to this model.
     *
     * @param $model
     *
     * @return void
     */
    protected function assignBookingType($model): void
    {
        $model->booking_type = BookingType::TAXI;
    }
}
