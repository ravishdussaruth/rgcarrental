<?php

namespace Lit\Config\Crud;

use App\Enums\BookingType;
use App\Models\Booking;
use App\Models\User;
use Ignite\Crud\CrudColumnBuilder;
use Ignite\Crud\CrudIndex;
use Ignite\Crud\CrudIndexTable;
use Ignite\Crud\CrudShow;
use Lit\Config\Crud\Concerns\BaseBookingConfig;
use Lit\Http\Controllers\Crud\TaxiBookingController;

class TaxiBookingConfig extends BaseBookingConfig
{
    /**
     * Controller class.
     *
     * @var string
     */
    public $controller = TaxiBookingController::class;

    /**
     * Model singular and plural name.
     *
     * @param Booking|null booking
     *
     * @return array
     */
    public function names(Booking $booking = null)
    {
        return [
            'singular' => 'Taxi',
            'plural'   => 'Taxis',
        ];
    }

    /**
     * Get crud route prefix.
     *
     * @return string
     */
    public function routePrefix()
    {
        return 'taxi-bookings';
    }

    /**
     * Build index page.
     *
     * @param \Ignite\Crud\CrudIndex $page
     *
     * @return CrudIndexTable
     */
    protected function table(CrudIndex $page): CrudIndexTable
    {
        return $page->table(function (CrudColumnBuilder $table) {
            $table->col('Reference')
                  ->value('reference');

            $table->col('User')
                  ->value('{user.first_name} {user.last_name}');

            $table->col('Pickup Address')
                  ->value('pickup_address');

            $table->col('Drop Address')
                  ->value('drop_address');

            $table->col('Pickup time')
                  ->value('pickup_time');

            $table->col('Approved')->value('approved', [
                true  => '<div class="badge badge-success">Approved</div>',
                false => '<div class="badge badge-danger">Waiting for approval</div>',
            ])->sortBy('approved');
        })->query(function ($query) {
            $query->select(['id', 'reference', 'user_id', 'pickup_address', 'drop_address', 'pickup_time', 'approved', 'created_at', 'updated_at'])
                  ->with(['user:id,first_name,last_name'])
                  ->where('booking_type', BookingType::TAXI);
        });
    }

    /**
     * The booking form.
     *
     * @param CrudShow $page
     *
     * @return void
     */
    protected function form(CrudShow $page)
    {
            $page->dt('pickup_time')
                 ->title('Pickup Time')
                 ->formatted('llll')
                 ->hint('Choose Pickup Time')
                 ->width(1 / 2);

            $page->input('no_of_passenger')
                 ->title('No of passenger')
                 ->type('number')
                 ->width(1 / 2);

            $page->datetime('returned_on')
                 ->title('Completed on')
                 ->formatted('llll')
                 ->width(1 / 2);

            $page->text('pickup_address')
                 ->title('Pickup Address')
                 ->width(1 / 2);

            $page->text('drop_address')
                 ->title('Drop Address')
                 ->width(1 / 2);

            $page->onlyOnUpdate(function ($form) {
                $form->textarea('rejected_comment')
                     ->title('Add comment if booking is being rejected');
            });
    }
}
