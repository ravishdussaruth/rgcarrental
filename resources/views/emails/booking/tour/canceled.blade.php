@extends('emails._partial')

@section('content')
    <img src="{{  asset('images/cancel.svg') }}" width="30%"/>

    <span class="font-weight-bold" style="font-size: 24px;">Sorry! Your tour booking has been rejected</span>

    @if(!empty($booking->rejected_comment))
        <p><span style="font-weight: 600;">Comment: </span> {{ $booking->rejected_comment }}</p>
    @endif

    <span>Contact RgCarRental Admin for more info.</span>
@endsection
