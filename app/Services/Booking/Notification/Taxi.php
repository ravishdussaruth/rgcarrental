<?php

namespace App\Services\Booking\Notification;

use Lit\Models\User;
use App\Contracts\Home;
use App\Models\Booking;
use Illuminate\Support\Facades\Notification;
use App\Notifications\Booking\Taxi\NotifyUserBookingCreated;
use App\Notifications\Booking\Taxi\NotifyUserBookingCanceled;
use App\Notifications\Booking\Taxi\NotifyUserBookingCompleted;
use App\Notifications\Booking\Taxi\NotifyOwnerAboutNewBooking;
use App\Notifications\Booking\Taxi\NotifyUserBookingConfirmed;
use App\Contracts\Booking\Notification as NotificationInterface;

class Taxi implements NotificationInterface
{
    /**
     * Notify owner about booking.
     *
     * @param Booking $booking
     *
     * @return void
     */
    public function notifyOwnerAboutBooking(Booking $booking)
    {
        resolve(Home::class)->admins()
                            ->each(fn(User $user) => $user->notify(NotifyOwnerAboutNewBooking::make($booking)));
    }

    /**
     * Notify user booking has been created.
     *
     * @param Booking $booking
     *
     * @return void
     */
    public function notifyCreated(Booking $booking)
    {
        Notification::send($booking->user, NotifyUserBookingCreated::make($booking));
    }

    /**
     * Notify user about booking has been confirmed.
     *
     * @param Booking $booking
     *
     * @return void
     */
    public function notifyConfirmed(Booking $booking)
    {
        Notification::send($booking->user, NotifyUserBookingConfirmed::make($booking));
    }

    /**
     * Notify user about booking has been canceled.
     *
     * @param Booking $booking
     *
     * @return void
     */
    public function notifyCanceled(Booking $booking)
    {
        Notification::send($booking->user, NotifyUserBookingCanceled::make($booking));
    }

    /**
     * Notify user about booking have been completed.
     *
     * @param Booking $booking
     *
     * @return void
     */
    public function notifyCompleted(Booking $booking)
    {
        Notification::send($booking->user, NotifyUserBookingCompleted::make($booking));
    }
}
