<?php

use App\Support\Migrationable;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromotionalOffersTable extends Migration
{
    use Migrationable;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotional_offers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('discount_id');
            $table->string('code')->unique();
            $table->timestamp('start_on');
            $table->timestamp('end_on')->nullable();
            $table->boolean('is_active')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });

        if ($this->isNotTesting()) {
            Schema::table('promotional_offers', function (Blueprint $table) {
                $table->foreign('discount_id')
                    ->references('id')
                    ->on('discounts');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ($this->isNotTesting()) {
            Schema::table('promotional_offers', function (Blueprint $table) {
                $table->dropForeign(['discount_id']);
            });
        }

        Schema::dropIfExists('promotional_offers');
    }
}
