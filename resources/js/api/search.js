import axios from 'axios';

/**
 * Search for cars.
 *
 * @param params
 *
 * @return {Promise<AxiosResponse<any>>}
 */
export const searchCar = (params) => axios.get('/search', { params });

/**
 * Search for user destinations.
 * 
 * @param {Object} params 
 * 
 * @return {Promise}
 */
export const searchDestinations = (params) => axios.get('/search-destinations', { params });