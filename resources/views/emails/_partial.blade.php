<!doctype html>
<html>
<head>
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&lang=en"/>

    <link rel="stylesheet" href="{{ mix('css/app.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/mdb.min.css') }}"/>
    <style>
        html {
            font-family: 'Open Sans', serif;
        }

        .email-container {
            display: flex;
            flex-direction: column !important;
            align-items: center;
            padding: 70px;
        }

        .font-weight-bold {
            font-weight: 700 !important;
        }

        .font-weight-normal {
            font-weight: 400 !important;
        }

        .font-weight-bolder {
            font-weight: bolder !important;
        }

        .single-footer-txt p {
            margin-bottom: 8px;
        }

        .single-footer-txt p a {
            color: #565a5c;
            font-size: 16px;
            transition: .5s ease-in-out;
            text-transform: capitalize;
        }

        .single-footer-txt p a:hover {
            -webkit-transition: scaleX(1.2);
            -moz-transition: scaleX(1.2);
            -ms-transition: scaleX(1.2);
            -o-transition: scaleX(1.2);
            transform: scaleX(1.2);
            color: #00d8ff;
        }
    </style>
</head>
<body>
<div class="email-container">
    @yield('content')
</div>

@include('emails.footer')
</body>
</html>
