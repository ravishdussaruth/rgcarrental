<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Car extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'               => $this->id,
            'name'             => $this->name,
            'transmission'     => $this->tranmissino,
            'car_cc'           => $this->car_cc,
            'color'            => $this->color,
            'ratings'          => $this->ratings,
            'price'            => $this->price,
            'rate_type'        => $this->rate_type,
            'description'      => $this->description,
            'features'         => $this->features,
            'max_seats'        => $this->max_seats,
            'image'            => $this->image,
            'car_transmission' => $this->car_transmission,
            'car_rate'         => $this->car_rate,
        ];
    }
}
