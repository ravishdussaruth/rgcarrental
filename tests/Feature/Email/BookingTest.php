<?php

namespace Tests\Feature\Email;

use App\Models\Car;
use App\Notifications\Booking\Rental\NotifyUserBookingCompleted;
use Tests\TestCase;
use App\Models\User;
use App\Models\Booking;
use App\Models\Location;
use App\Enums\BookingType;
use Illuminate\Support\Facades\Notification;
use App\Notifications\Booking\Rental\NotifyUserBookingCanceled;
use App\Notifications\Booking\Rental\NotifyUserBookingConfirmed;

class BookingTest extends TestCase
{
    /**
     * Test that an email is sent to the user when booking has been approved.
     *
     * @return void
     */
    public function testEmailIsSentWhenCarRentalBookingIsApproved()
    {
        Notification::fake();

        // Given a user, and their booking.
        $user = User::factory()->create();

        $car = Car::factory()->create();

        $booking = Booking::factory()->create([
            'user_id'      => $user->id,
            'car_id'       => $car->id,
            'booking_type' => BookingType::RENT,
            'pickup_id'    => Location::factory()->create()->id,
            'drop_id'      => Location::factory()->create()->id,
            'pickup_time'  => '2021-11-11 20:20:00',
            'drop_time'    => '2021-11-14 20:20:00',
            'approved'     => false,
            'reference'    => generate_booking_id()
        ]);

        // When booking has been approved.
        $booking->update(['approved' => true]);

        Notification::assertSentTo($user, NotifyUserBookingConfirmed::class);
    }

    /**
     * Test that an email is sent to the user when booking has been canceled.
     *
     * @return void
     */
    public function testEmailIsSentWhenCarRentalBookingIsCanceled()
    {
        Notification::fake();

        // Given a user, and their booking.
        $user = User::factory()->create();

        $car = Car::factory()->create();

        $booking = Booking::factory()->create([
            'user_id'      => $user->id,
            'car_id'       => $car->id,
            'booking_type' => BookingType::RENT,
            'pickup_id'    => Location::factory()->create()->id,
            'drop_id'      => Location::factory()->create()->id,
            'pickup_time'  => '2021-11-11 20:20:00',
            'drop_time'    => '2021-11-14 20:20:00',
            'approved'     => true,
            'reference'    => generate_booking_id()
        ]);

        // When booking has been canceled.
        $booking->update(['approved' => false]);

        Notification::assertSentTo($user, NotifyUserBookingCanceled::class);
    }

    /**
     * Test email is sent when admin confirm booking has been completed.
     *
     * @return void
     */
    public function testEmailIsSentWhenCarRentalBookingIsCompleted()
    {
        Notification::fake();

        // Given a user, and their booking.
        $user = User::factory()->create();

        $car = Car::factory()->create();

        $booking = Booking::factory()->create([
            'user_id'      => $user->id,
            'car_id'       => $car->id,
            'booking_type' => BookingType::RENT,
            'pickup_id'    => Location::factory()->create()->id,
            'drop_id'      => Location::factory()->create()->id,
            'pickup_time'  => '2021-11-11 20:20:00',
            'drop_time'    => '2021-11-14 20:20:00',
            'approved'     => true,
            'reference'    => generate_booking_id()
        ]);

        // When booking has been completed.
        $booking->update(['returned_on' => now()]);

        Notification::assertSentTo($user, NotifyUserBookingCompleted::class);
    }
}
