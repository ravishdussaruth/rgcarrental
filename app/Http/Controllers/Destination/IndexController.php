<?php

namespace App\Http\Controllers\Destination;

use App\Models\Destination;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use App\Enums\User\Destination as Enum;
use App\Http\Resources\Destination as ResourcesDestination;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * Display all destination for this category.
     *
     * @param string $category
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(string $category)
    {
        $destinations = $this->destinations($category);

        return view('destinations.index', compact('destinations'));
    }

    /**
     * Search destination in this spefic region.
     * 
     * @param Request $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        return ResourcesDestination::collection($this->destinations(Enum::getDescription((int)$request->category)));
    }

    /**
     * Search for destination in this category.
     * 
     * @param string $region
     * 
     * @return Collection
     */
    protected function destinations(string $region)
    {
        try {
            $key = Enum::fromKey(strtoupper($region))->value;
        } catch (\Exception $e) {
            $key = 0;
        }

        return Destination::whereCategory($key)->get();
    }
}
