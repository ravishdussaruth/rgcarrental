<?php

namespace App\Notifications\Booking\Tour;

use App\Notifications\Booking\Concerns\BookingNotification;

class NotifyUserBookingConfirmed extends BookingNotification
{
    /**
     * The view path.
     *
     * @return string
     */
    protected function view(): string
    {
        return 'emails.booking.tour.confirmed';
    }

    /**
     * The email subject.
     *
     * @return string
     */
    protected function subject(): string
    {
        return 'Tour Booking Confirmed';
    }
}
