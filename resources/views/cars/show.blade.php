@extends('partials.app', ['position' => 'position:relative'])

@section('title')
  {{ $car->name }}
@endsection

@section('header')
  {!! SEO::generate() !!}
@endsection

@section('app')
  <div class="p-lg-5 mt-lg-5 p-3 container">
    <div class="d-flex flex-column flex-lg-row col-12 justify-content-between mb-2">
      <carousel class="col-lg-6 col-12" :medias="{{ $media }}"></carousel>
      
      <div class="col-lg-6 col-12">
        <span class="font-weight-bold">Description</span>
        <hr>
        <p class="text-gray">{{ $car->description }}</p>
      </div>
    </div>
    
    <span class="font-weight-bold mt-2">Options</span>
    <hr>
    
    <div class="col-12 mb-2 d-flex flex-wrap justify-content-between mt-2">
      <icon class="mt-2 ml-0 col"
            :image-style="{ width: '2rem', height: '2rem' }"
            src="engine.svg"
            text-classes="h6"
            text="{{ $car->car_cc }}">
      </icon>
      
      <icon class="mt-2 col"
            :image-style="{ width: '2rem', height: '2rem' }"
            src="gearshift.svg"
            text-classes="h6"
            text="{{ $car->car_transmission }}">
      </icon>
      
      <icon class="mt-2 col"
            :image-style="{ width: '2rem', height: '2rem' }"
            src="color.svg"
            text-classes="h6"
            text="{{ $car->color }}">
      </icon>
      
      <icon class="mt-2 col"
            :image-style="{ width: '2rem', height: '2rem' }"
            src="car-seat.svg"
            text-classes="h6"
            text="{{ $car->max_seats }}">
      </icon>
    </div>
    
    <span class="font-weight-bold mt-2">Features</span>
    <hr>
    <div class="d-flex col-12 flex-wrap">
      @foreach($features as $feature)
        <div class="d-flex align-items-center col-6 col-lg-2 p-0" style="opacity: 0.7;">
          <b-icon-check font-scale="2"></b-icon-check>
          <span class="text-gray">{{ $feature->name }}</span>
        </div>
      @endforeach
    </div>
    
    <span class="font-weight-bold mt-2">Booking information</span>
    <hr>
    
    <car-rent pickup-date="{{ request()->query('pickup_time') }}"
              drop-date="{{ request()->query('drop_time') }}"
              :car="{{ $car }}">
    </car-rent>
    
    <hr>
    @if($otherCars->isNotEmpty())
      <span class="font-weight-bold mt-2">Other Cars In Our Selection</span>
      <hr>
      <list :cars="{{ $otherCars->toJson() }}"></list>
    @endif
  </div>
@endsection
