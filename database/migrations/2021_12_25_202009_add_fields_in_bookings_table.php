<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsInBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->integer('booking_type')
                  ->nullable()
                  ->after('approved');

            $table->string('flight_number')
                  ->nullable()
                  ->after('booking_type');

            $table->integer('no_of_passenger')
                  ->nullable()
                  ->after('flight_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn([
                'booking_type',
                'flight_number',
                'no_of_passenger'
            ]);
        });
    }
}
