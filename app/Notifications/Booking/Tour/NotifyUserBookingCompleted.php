<?php

namespace App\Notifications\Booking\Tour;

use Illuminate\Support\Facades\URL;
use App\Notifications\Booking\Concerns\BookingNotification;

class NotifyUserBookingCompleted extends BookingNotification
{
    /**
     * The view path.
     *
     * @return string
     */
    protected function view(): string
    {
        return 'emails.booking.tour.feedback';
    }

    /**
     * The params for the notification.
     *
     * @return array
     */
    protected function params(): array
    {
        return array_merge(
            parent::params(),
            [
                'feedbackUrl' => URL::temporarySignedRoute(
                    'feedback', now()->addMinutes(30), ['booking' => $this->booking->reference]
                )
            ]
        );
    }

    /**
     * The email subject.
     *
     * @return string
     */
    protected function subject(): string
    {
        return 'Tour Booking Completed';
    }
}
