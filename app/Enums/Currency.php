<?php

namespace App\Enums;

use Illuminate\Support\Collection;

class Currency extends Enum
{
    /**
     * American dollar.
     *
     * @var int
     */
    const USD = 1;

    /**
     * Euro.
     *
     * @var int
     */
    const EUR = 2;

    /**
     * Mauritian rupee.
     *
     * @var int
     */
    const MUR = 3;

    /**
     * British pound.
     *
     * @var int
     */
    const GBP = 4;

    /**
     * The currencies symbol.
     *
     * @return Collection
     */
    public static function symbols(): Collection
    {
        return collect([
            self::USD => '$',
            self::EUR => '€',
            self::MUR => 'Rs',
            self::GBP => '£'
        ]);
    }
}
