<?php

namespace App\Notifications\Booking\Rental;

use App\Notifications\Booking\Concerns\BookingNotification;

class NotifyUserBookingConfirmed extends BookingNotification
{
    /**
     * The view path.
     *
     * @return string
     */
    protected function view(): string
    {
        return 'emails.booking.rental.confirmed';
    }

    /**
     * The email subject.
     *
     * @return string
     */
    protected function subject(): string
    {
        return 'Car Booking Confirmed';
    }
}
