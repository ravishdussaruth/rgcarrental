<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Model;

class Tour extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description', 'price'];

    /**
     * The destinations for this tour.
     * 
     * @return HasMany
     */
    public function destinations(): HasMany
    {
        return $this->hasMany(Destination::class);
    }
}
