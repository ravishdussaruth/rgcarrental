<?php

namespace App\Models\Concerns;

trait HasBookingUpdated
{
    /**
     * Will tell us if this booking has been confirmed by admin.
     *
     * @return bool
     */
    public function isConfirmed(): bool
    {
        return $this->approved && !is_null($this->user->email);
    }

    /**
     * Will tell us if this booking has been canceled by admin.
     *
     * @return bool
     */
    public function isCanceled(): bool
    {
        return !$this->approved && !is_null($this->user->email);
    }

    /**
     * Will tell us if original price has been updated.
     *
     * @return bool
     */
    public function originalPriceJustChanged()
    {
        return $this->isDirty('original_price');
    }

    /**
     * Approved field just changes.
     *
     * @return bool
     */
    public function approvedJustChanged(): bool
    {
        return $this->isDirty(['approved']) && is_null($this->returned_on);
    }

    /**
     * Will tell us if discount has been applied.
     *
     * @return bool
     */
    public function discountJustChanged(): bool
    {
        return $this->isDirty('discount_id');
    }

    /**
     * Will tell us if the booking has been completed.
     *
     * @return bool
     */
    public function bookingCompleted(): bool
    {
        return $this->isDirty('returned_on') && !is_null($this->returned_on);
    }
}
