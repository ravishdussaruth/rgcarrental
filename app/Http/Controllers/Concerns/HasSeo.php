<?php

namespace App\Http\Controllers\Concerns;

use App\Contracts\Home;
use Artesaos\SEOTools\Facades\SEOMeta;
use Facades\Artesaos\SEOTools\SEOTools;

trait HasSeo
{
    protected function render(Home $home)
    {
        SEOTools::setTitle(__('seo.home.title'));
        SEOTools::setDescription(__('seo.home.description'));
        SEOTools::opengraph()->setUrl(request()->fullUrl());
        SEOTools::setCanonical(request()->fullUrl());
        SEOTools::opengraph()->addProperty('type', 'website');
        SEOMeta::setKeywords([
            ...$home->contactNumbers(),
            ...$home->seo(),
            $home->address(),
            $home->email(),
            'mauritius', 'cheap prices', 'airport transfer', 'taxi', 'tour', 'rental', 'car rental', 'rajeskumar', 'rajes goreeba', 'suzuki', 'suzuki celerio', 'grand port',
        ]);
    }
}