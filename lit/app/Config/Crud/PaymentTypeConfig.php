<?php

namespace Lit\Config\Crud;

use App\Models\PaymentType;
use Ignite\Crud\Config\CrudConfig;
use Ignite\Crud\CrudIndex;
use Ignite\Crud\CrudShow;
use Lit\Http\Controllers\Crud\PaymentTypeController;

class PaymentTypeConfig extends CrudConfig
{
    /**
     * Model class.
     *
     * @var string
     */
    public $model = PaymentType::class;

    /**
     * Controller class.
     *
     * @var string
     */
    public $controller = PaymentTypeController::class;

    /**
     * Model singular and plural name.
     *
     * @param PaymentType|null paymentType
     *
     * @return array
     */
    public function names(PaymentType $paymentType = null)
    {
        return [
            'singular' => 'Payment Type',
            'plural'   => 'Payment Types',
        ];
    }

    /**
     * Get crud route prefix.
     *
     * @return string
     */
    public function routePrefix()
    {
        return 'payment-types';
    }

    /**
     * Build index page.
     *
     * @param \Ignite\Crud\CrudIndex $page
     *
     * @return void
     */
    public function index(CrudIndex $page)
    {
        $page->table(function ($table) {
            $table->col('Type')->value('{name}');
        })->search('title');
    }

    /**
     * Setup show page.
     *
     * @param \Ignite\Crud\CrudShow $page
     *
     * @return void
     */
    public function show(CrudShow $page)
    {
        $page->card(function ($form) {
            $form->input('name')
                 ->title('Payment Type');
        });
    }
}
