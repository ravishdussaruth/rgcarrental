<?php

namespace Deployer;

require 'recipe/laravel.php';

// Overriding default time out.
set('default_timeout', 2000);

// Project name
set('application', 'Rg Car Rental');

// Set Deploy Path.
set('deploy_path', '/home/websites/rgcarrental');

// We do not want recursive cloning.
set('git_recursive', false);

// Number of Releases to Keep.
set('keep_releases', 3);

// Project repository
set('repository', 'https://gitlab.com/ravishdussaruth/rgcarrental.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', false);

// Set inventory file.
inventory('hosts.yml');

// Unnecessary files and folders.
set('unwanted_assets', [
    '.docker',
    'tests',
    '.env.example',
    '.env.testing',
    'containers',
    'docker-compose.yml',
    '.gitattributes',
    '.gitignore',
    '.gitlab-ci.yml',
    '.gitmodules',
    '.php_cs.dist',
    'phpunit.xml',
    'readme.md',
    'deploy.php',
    'server.php',
    'composer.phar',
    'deployer.phar',
    'public/js/build',
    'hosts.yml',
    'phpstan.neon',
    'CHANGELOG.md'
]);

// Shared files/dirs between deploys
add('shared_dirs', []);

// Writable dirs by web server
add('writable_dirs', []);
set('allow_anonymous_stats', false);

// Default Deploy Stage.
set('default_stage', 'dev');

// Install Npm dependencies.
desc('Install node dependencies');
task('npm:install', function () {
    run('cd {{release_path}} && yarn');
});

// Build Assets.
desc('Build npm assets');
task('npm:build', function () {
    run('cd {{release_path}} && npm run {{npm_build_options}}', [
        'timeout' => 50000
    ]);
});

// Clean Code base.
desc('Clean code base by removing unnecessary files and folders');
task('clean:code-base', function () {
    // Get unwanted assets.
    $unwantedAssets = join(' ', get('unwanted_assets'));
    // No assets.
    if (empty($unwantedAssets)) {
        return;
    }

    // Remove unwanted assets.
    run('cd {{release_path}} && $sudo rm -rf ' . $unwantedAssets);
});

// Run Install node dependencies.
after('deploy:vendors', 'npm:install');

// Build Npm dependencies.
after('npm:install', 'npm:build');

// Run Route cache after config cache.
after('artisan:config:cache', 'artisan:route:cache');

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.
before('deploy:symlink', 'artisan:migrate');

// After cleaning up old releases we
// remove unwanted assets from
// Production.
after('cleanup', 'clean:code-base');

// Reload PHP-FPM service.
after('clean:code-base', 'supervisorctl:reload');
