<?php

namespace App\Enums;

use App\Services\Booking\Notification\Tour;
use Illuminate\Support\Collection;
use App\Contracts\Booking\Notification;
use App\Services\Booking\Notification\Taxi;
use App\Services\Booking\Notification\Rental;

class BookingType extends Enum
{
    /**
     * User is booking a tour.
     *
     * @var int
     */
    const TOUR = 1;

    /**
     * User is booking for a taxi.
     *
     * @var int
     */
    const TAXI = 2;

    /**
     * User is renting a car.
     *
     * @var int
     */
    const RENT = 3;

    /**
     * The notification for the booking types.
     *
     * @return Collection
     */
    protected static function notificationClasses(): Collection
    {
        return collect([
            self::TOUR => Tour::class,
            self::RENT => Rental::class,
            self::TAXI => Taxi::class
        ]);
    }

    /**
     * Retrieve the notification for this booking type.
     *
     * @param int $bookingType
     *
     * @return Notification
     */
    public static function notificationFor(int $bookingType): ?Notification
    {
        if (!self::notificationClasses()->has($bookingType)) {
            return null;
        }

        return new (self::notificationClasses()->get($bookingType));
    }

    /**
     * The routes crud routes for the bookings.
     *
     * @return Collection
     */
    public static function routes(): Collection
    {
        return collect([
            self::RENT => 'lit.crud.booking.show',
            self::TAXI => 'lit.crud.taxi_booking.show',
            self::TOUR => 'lit.crud.tour_booking.show'
        ]);
    }
}
