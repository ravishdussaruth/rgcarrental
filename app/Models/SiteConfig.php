<?php

namespace App\Models;

use Ignite\Crud\Models\Traits\HasMedia;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia as HasMediaContract;

class SiteConfig extends Model implements HasMediaContract
{
    use HasMedia;
}
