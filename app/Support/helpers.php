<?php

use Illuminate\Support\Str;

if (!function_exists('generate_booking_id')) {
    /**
     * Generate booking id.
     *
     * @return string
     */
    function generate_booking_id(): string
    {
        return '#B' . strtoupper(substr(now()->format('D'), 0, 2)) . str_shuffle(now()->timestamp . Str::random(4));
    }
}

if (!function_exists('service')) {
    /**
     * Generate booking id.
     *
     * @return mixed
     */
    function service(string $key)
    {
        return config("services.$key");
    }
}
