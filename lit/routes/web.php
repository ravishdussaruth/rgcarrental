<?php

use Illuminate\Support\Facades\Route;
use Lit\Http\Controllers\WelcomeController;

Route::get('/', WelcomeController::class);

Route::get('/website-config', \Lit\Http\Controllers\WebsiteConfigController::class)
     ->name('app.config');
