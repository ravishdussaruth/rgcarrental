<?php

namespace App\Notifications\Booking\Rental;

use App\Notifications\Booking\Concerns\BookingNotification;

class NotifyUserBookingCreated extends BookingNotification
{
    /**
     * The view path.
     *
     * @return string
     */
    protected function view(): string
    {
        return 'emails.booking.rental.created';
    }

    /**
     * The email subject.
     *
     * @return string
     */
    protected function subject(): string
    {
        return 'Booking Received';
    }
}
