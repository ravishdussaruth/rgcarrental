<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware('page-cache')
     ->get('/', \App\Http\Controllers\Page\IndexController::class)
     ->name('home');

Route::view('feedback', 'feedbacks.index')
     ->name('feedback')
     ->middleware('signed');

Route::post('submit-feedback', \App\Http\Controllers\FeedbackController::class)
     ->name('submit-feedback');

Route::get('/taxi-booking', \App\Http\Controllers\Page\TaxiController::class)->name('taxi.booking');
Route::get('/tour-booking', \App\Http\Controllers\Page\TourController::class)->name('tour.booking');

Route::get('cars/search', [\App\Http\Controllers\Booking\AvailableCarController::class, 'search'])->name('show-car');
Route::get('cars/{car:slug}', [\App\Http\Controllers\Car\IndexController::class, 'show'])->name('car.show');

Route::get('search-destinations', [\App\Http\Controllers\Destination\IndexController::class, 'search'])->name('search.destination');

Route::post('/book', [\App\Http\Controllers\Booking\BookingController::class, 'store'])
     ->name('app.book');

Route::get('address', \App\Http\Controllers\Address\IndexController::class)
     ->name('app.address.search');

Route::post('/validate-coupon', \App\Http\Controllers\CouponDiscountController::class)->name('app.validate.coupon');
Route::post('/check-price', \App\Http\Controllers\DiscountController::class)->name('app.check.price');

Route::get('destination/{category}', [\App\Http\Controllers\Destination\IndexController::class, 'index'])->name('app.destinations');
