<?php

namespace App\Contracts;

use Illuminate\Support\Collection;

interface Home
{
    /**
     * The cover image.
     *
     * @return ?string
     */
    public function cover(): ?string;

    /**
     * The website logo.
     *
     * @return ?string
     */
    public function logo(): ?string;

    /**
     * The website slogan.
     *
     * @return ?string
     */
    public function slogan(): ?string;

    /**
     * RGCarRental admins.
     *
     * @return Collection
     */
    public function admins(): Collection;

    /**
     * The contact numbers.
     * 
     * @return Collection
     */
    public function contactNumbers(): Collection;

    /**
     * The seo metas.
     *
     * @return array
     */
    public function seo(): array;

    /**
     * The company email address.
     * 
     * @return string
     */
    public function email(): string;

    /**
     * The company address.
     * 
     * @return string
     */
    public function address(): string;
}
