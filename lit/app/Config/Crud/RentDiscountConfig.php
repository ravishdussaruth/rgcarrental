<?php

namespace Lit\Config\Crud;

use App\Models\Discount;
use App\Models\RentDiscount;
use Ignite\Crud\Config\CrudConfig;
use Ignite\Crud\CrudIndex;
use Ignite\Crud\CrudShow;
use Illuminate\Database\Eloquent\Builder;
use Lit\Http\Controllers\Crud\RentDiscountController;

class RentDiscountConfig extends CrudConfig
{
    /**
     * Model class.
     *
     * @var string
     */
    public $model = RentDiscount::class;

    /**
     * Controller class.
     *
     * @var string
     */
    public $controller = RentDiscountController::class;

    /**
     * Model singular and plural name.
     *
     * @param  RentDiscount|null rentDiscount
     *
     * @return array
     */
    public function names(RentDiscount $rentDiscount = null)
    {
        return [
            'singular' => 'Rent Discount',
            'plural' => 'Rent Discounts',
        ];
    }

    /**
     * Get crud route prefix.
     *
     * @return string
     */
    public function routePrefix()
    {
        return 'rent-discounts';
    }

    /**
     * Build index page.
     *
     * @param  \Ignite\Crud\CrudIndex  $page
     *
     * @return void
     */
    public function index(CrudIndex $page)
    {
        $page->table(function ($table) {
            $table->col('Min Day')->value('{min_day}');
            $table->col('Max Day')->value('{max_day}');
            $table->col('Discount')->value('{discount.name}');
        })
             ->query(fn(Builder $query) => $query->with('discount'));
    }

    /**
     * Setup show page.
     *
     * @param  \Ignite\Crud\CrudShow  $page
     *
     * @return void
     */
    public function show(CrudShow $page)
    {
        $page->card(function ($form) {
            $form->input('min_day')
                 ->title('Min Day')
                 ->type('number');

            $form->input('max_day')
                 ->title('Max Day')
                 ->type('number');

            $form->select('discount_id')
                 ->title('Discount')
                 ->options(
                     Discount::all()->mapWithKeys(fn(Discount $discount) => [$discount->id => $discount->name])->toArray()
                 );
        });
    }
}
