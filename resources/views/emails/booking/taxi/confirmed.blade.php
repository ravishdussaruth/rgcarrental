@extends('emails._partial')

@section('content')
    <img src="{{  asset('images/taxi.svg') }}" width="30%"/>

    <span class="font-weight-bold" style="font-size: 24px;">Yaaay! Your taxi booking has been confirmed</span>

    <span class="font-weight-normal" style="font-size: 16px;">Your Booking Info</span>

    <table class="table">
        <thead>
        <tr>
            <th scope="col"></th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Reference</td>
            <td>{{ $booking->reference }}</td>
        </tr>

        <tr>
            <td>Pick Up Address</td>
            <td>{{ $booking->pickup_address }}</td>
        </tr>

        <tr>
            <td>Drop Address</td>
            <td>{{ $booking->drop_address }}</td>
        </tr>

        <tr>
            <td>Pick Up Date</td>
            <td>{{ $booking->pickup_time }}</td>
        </tr>

        <tr>
            <td>Comment</td>
            <td>{{ $booking->rejected_comment }}</td>
        </tr>
        </tbody>
    </table>
@endsection
