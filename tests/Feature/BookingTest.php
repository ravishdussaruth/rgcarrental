<?php

namespace Tests\Feature;

use App\Models\Car;
use Tests\TestCase;
use App\Models\User;
use App\Models\Booking;
use App\Models\Location;
use App\Enums\User\Titles;
use App\Enums\BookingType;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use App\Enums\Vehicle\Transmissions;
use Illuminate\Support\Facades\Notification;
use App\Notifications\Booking\Rental\NotifyOwnerAboutNewBooking;

class BookingTest extends TestCase
{
    /**
     * Test that a user can make a booking.
     *
     * @return void
     */
    public function testBookingIsMade()
    {
        $admin = \Lit\Models\User::factory()->create();

        Notification::fake();

        // Given a user.
        /** @var User $user */
        $user = User::factory()->create([
            'title'             => 'Mr',
            'first_name'        => 'Ravish',
            'last_name'         => 'Dussaruth',
            'phone_number'      => '4398509358490684096',
            'email'             => 'ravishdussaruth@gmail.com',
            'email_verified_at' => now(),
            'password'          => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            'remember_token'    => Str::random(10),
        ]);

        $booking = [
            'car_id'       => Car::factory()->create()->id,
            'pickup_time'  => Carbon::now()->format('Y-m-d H:i'),
            'drop_time'    => Carbon::now()->addDays(2)->format('Y-m-d H:i'),
            'pickup_id'    => Location::factory()->create()->id,
            'drop_id'      => Location::factory()->create()->id,
            'booking_type' => BookingType::RENT,
            'price'        => 2000,
            'user'         => [
                'title'        => Titles::MR,
                'first_name'   => 'Ravish',
                'last_name'    => 'Dussaruth',
                'phone_number' => '4398509358490684096',
                'email'        => 'ravishdussaruth@gmail.com',
            ]
        ];

        $response = $this->postJson(route('app.book'), $booking);
        $response->assertSuccessful();

        $createdUser = User::first();

        $this->assertDatabaseHas('bookings', [
            'user_id'        => $createdUser->id,
            'original_price' => $booking['price']
        ]);

        $this->assertCount(1, $createdUser->bookings);

        Notification::assertSentTo($admin, NotifyOwnerAboutNewBooking::class);
    }

    /**
     * Test that the user cannot make a booking if the car is not available.
     *
     * @return void
     */
    public function testUserCanBookOnlyViewCarWhichAreAvailable()
    {
        // Given 3 car, and a booking for 4 days
        $car = Car::factory()->automatic()
                  ->create([
                      'name' => 'Suzuki Alto'
                  ]);

        $car2 = Car::factory()->manual()
                   ->create([
                       'name' => 'Suzuki Elantra',
                   ]);

        Car::factory()->automatic()
           ->create([
               'name' => 'Suzuki Tucson',
           ]);

        $user = User::factory()->create();

        Booking::factory()->create([
            'user_id'      => $user->id,
            'car_id'       => $car->id,
            'pickup_id'    => Location::factory()->create()->id,
            'drop_id'      => Location::factory()->create()->id,
            'pickup_time'  => '2021-11-06 20:20:00',
            'drop_time'    => '2021-11-10 20:20:00',
            'booking_type' => BookingType::RENT,
            'approved'     => true,
            'reference'    => generate_booking_id()
        ]);

        Booking::factory()->create([
            'user_id'      => $user->id,
            'car_id'       => $car2->id,
            'pickup_id'    => Location::factory()->create()->id,
            'drop_id'      => Location::factory()->create()->id,
            'pickup_time'  => '2021-11-11 20:20:00',
            'drop_time'    => '2021-11-14 20:20:00',
            'booking_type' => BookingType::RENT,
            'approved'     => false,
            'reference'    => generate_booking_id()
        ]);

        $response = $this->getJson(route('app.search.car', [
            'drop_time'   => '2021-11-08 00:00',
            'pickup_time' => '2021-11-05 00:00',
        ]));

        $response->assertSuccessful();

        $this->assertCount(2, $response->json('data'));
    }

    /**
     * Test user wants to book a manual car.
     *
     * @return void
     */
    public function testUserWantsOnlyAManualCar()
    {
        $car = Car::factory()->automatic()
                  ->create([
                      'name' => 'Suzuki Alto'
                  ]);

        $car2 = Car::factory()->manual()
                   ->create([
                       'name' => 'Suzuki Elantra',
                   ]);

        Car::factory()->manual()
           ->create([
               'name' => 'Suzuki Tucson',
           ]);

        $user = User::factory()->create();

        Booking::factory()->create([
            'user_id'      => $user->id,
            'car_id'       => $car->id,
            'pickup_id'    => Location::factory()->create()->id,
            'drop_id'      => Location::factory()->create()->id,
            'booking_type' => BookingType::RENT,
            'pickup_time'  => '2021-11-06 20:20:00',
            'drop_time'    => '2021-11-10 20:20:00',
            'approved'     => true,
            'reference'    => generate_booking_id()
        ]);

        Booking::factory()->create([
            'user_id'      => $user->id,
            'car_id'       => $car2->id,
            'pickup_id'    => Location::factory()->create()->id,
            'drop_id'      => Location::factory()->create()->id,
            'booking_type' => BookingType::RENT,
            'pickup_time'  => '2021-11-11 20:20:00',
            'drop_time'    => '2021-11-14 20:20:00',
            'approved'     => false,
            'reference'    => generate_booking_id()
        ]);


        $response = $this->getJson(route('app.search.car', [
            'drop_time'   => '2021-11-08 00:00',
            'pickup_time' => '2021-11-05 00:00',
            'car_type'    => Transmissions::MANUAL
        ]));

        $response->assertSuccessful();

        $this->assertCount(1, $response->json('data'));
    }
}
