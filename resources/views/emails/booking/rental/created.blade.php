@extends('emails._partial')

@section('content')
    <img src="{{  asset('images/success.svg') }}" width="30%"/>

    <p class="font-weight-bold" style="font-size: 24px;">Your booking has been placed. An admin will soon contact you to confirm your booking.</p>

    <span class="font-weight-normal" style="font-size: 16px;">Find your booking info below</span>

    <div class="d-flex flex-column mt-1">
        <h5 class="font-weight-bolder">{{ $booking->car->name }}</h5>
        <img src="{{ $booking->car->image->url }}">

        <div class="d-flex align-items-center justify-content-between p-1 mt-2">
            @include('emails.booking.rental.icon', ['source' => asset('images/engine.svg'), 'text' => $booking->car->cc])
            @include('emails.booking.rental.icon', ['source' => asset('images/gearshift.svg'), 'text' => $booking->car->car_transmission])
        </div>

        <div class="d-flex align-items-center justify-content-between p-1">
            @include('emails.booking.rental.icon', ['source' => asset('images/color.svg'), 'text' => $booking->car->color])
            @include('emails.booking.rental.icon', ['source' => asset('images/car-seat.svg'), 'text' => $booking->car->ratings])
        </div>
    </div>

    <span class="mt-1">Status: <span class="font-weight-bold">Pending</span></span>

    <table class="table mt-3" style="width: 50%;">
        <thead>
        <tr>
            <th scope="col"></th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Reference</td>
            <td>{{ $booking->reference }}</td>
        </tr>

        <tr>
            <td>Pick Up Location</td>
            <td>{{ $booking->pickupLocation->location_name }}</td>
        </tr>

        <tr>
            <td>Drop Location</td>
            <td>{{ $booking->dropLocation->location_name }}</td>
        </tr>

        <tr>
            <td>Pick Up Date</td>
            <td>{{ $booking->pickup_time }}</td>
        </tr>

        <tr>
            <td>Drop Date</td>
            <td>{{ $booking->drop_time }}</td>
        </tr>
        </tbody>
    </table>
@endsection
