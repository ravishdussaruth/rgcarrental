<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeCarIdToNullableInBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->unsignedBigInteger('car_id')
                  ->nullable()
                  ->change();

            $table->unsignedBigInteger('pickup_id')
                  ->nullable()
                  ->change();

            $table->unsignedBigInteger('drop_id')
                  ->nullable()
                  ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->unsignedBigInteger('car_id')
                  ->change();

            $table->unsignedBigInteger('pickup_id')
                  ->change();

            $table->unsignedBigInteger('drop_id')
                  ->change();
        });
    }
}
