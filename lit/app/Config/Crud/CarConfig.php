<?php

namespace Lit\Config\Crud;

use App\Enums\BookingType;
use App\Enums\Vehicle\RateTypes;
use App\Enums\Vehicle\Transmissions;
use App\Models\Booking;
use App\Models\Car;
use App\Models\CarType;
use Ignite\Crud\Config\CrudConfig;
use Ignite\Crud\CrudIndex;
use Ignite\Crud\CrudShow;
use Illuminate\Support\Str;
use Lit\Http\Controllers\Crud\CarController;

class CarConfig extends CrudConfig
{
    /**
     * Model class.
     *
     * @var string
     */
    public $model = Car::class;

    /**
     * Controller class.
     *
     * @var string
     */
    public $controller = CarController::class;

    /**
     * Model singular and plural name.
     *
     * @param Car|null car
     *
     * @return array
     */
    public function names(Car $car = null)
    {
        return [
            'singular' => 'Car',
            'plural'   => 'Cars',
        ];
    }

    /**
     * Get crud route prefix.
     *
     * @return string
     */
    public function routePrefix()
    {
        return 'cars';
    }

    /**
     * Build index page.
     *
     * @param \Ignite\Crud\CrudIndex $page
     *
     * @return void
     */
    public function index(CrudIndex $page)
    {
        $page->table(function ($table) {
            $table->image('Image')
                  ->src('{thumbnail.conversion_urls.sm}')
                  ->maxWidth('50px')
                  ->maxHeight('50px');

            $table->col('Name')
                  ->value('{name}')
                  ->sortBy('name');
        })->search('name');
    }

    /**
     * Setup show page.
     *
     * @param \Ignite\Crud\CrudShow $page
     *
     * @return void
     */
    public function show(CrudShow $page)
    {
        $page->card(function ($form) {
            $form->image('images')
                 ->title('Photos')
                 ->hint('Upload Car Photo')
                 ->maxFiles(10);

            $form->input('name')
                 ->width(1 / 2)
                 ->creationRules('required')
                 ->rules('min:2', 'max:255')
                 ->title('Car name');

            $form->select('transmission')
                 ->width(1 / 2)
                 ->creationRules('required')
                 ->title('Car transmission')
                 ->options(Transmissions::labels()->toArray());

            $form->select('car_type_id')
                 ->width(1 / 2)
                 ->creationRules('required')
                 ->title('Car Type')
                 ->options(CarType::all()->mapWithKeys(fn(CarType $carType) => [$carType->id => Str::upper($carType->name)])->toArray());

            $form->input('car_cc')
                 ->width(1 / 2)
                 ->creationRules('required')
                 ->title('Car CC');

            $form->input('max_seats')
                 ->width(1 / 2)
                 ->creationRules('required')
                 ->rules('max:60')
                 ->title('Max seats in car');

            $form->input('color')
                 ->width(1 / 2)
                 ->title('Color');

            $form->input('ratings')
                 ->width(1 / 2)
                 ->title('Ratings');

            $form->input('price')
                 ->width(1 / 2)
                 ->title('Price');

            $form->select('rate_type')
                 ->width(1 / 2)
                 ->creationRules('required')
                 ->title('Rate Type')
                 ->options(RateTypes::labels()->toArray());

            $form->textarea('description')
                 ->title('Description');
        });

        $page->onlyOnUpdate(function (CrudShow $page) {
            $page->card(function ($form) {
                $form->relation('features')
                     ->type('tags')
                     ->tagValue('{name}');
            });

            $page->card(function ($form) {
                $form->relation('bookings')
                     ->query(function ($query) {
                         $query->with(['pickupLocation', 'dropLocation'])
                               ->where('booking_type', BookingType::RENT);
                     })
                     ->preview(function ($table) {
                         $table->col('Reference')
                               ->value('reference');

                         $table->col('Pick Up Time')
                               ->value('pickup_time');

                         $table->col('Drop Time')
                               ->value('drop_time');

                         $table->col('Delivered On')
                               ->value('delivered_on');

                         $table->col('Returned On')
                               ->value('returned_on');
                     });
            });
        });
    }
}
