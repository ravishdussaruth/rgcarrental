<?php

namespace App\Models\Concerns;

trait HasDiscount
{
    /**
     * Apply discount.
     *
     * @return void
     */
    public function applyDiscount(): void
    {
        $this->updateQuietly([
            'price' => $this->original_price * (1 - $this->discount->coeff)
        ]);
    }

    /**
     * Reset displayed price to original price.
     *
     * @return void
     */
    public function resetPriceToOriginalPrice(): void
    {
        $this->updateQuietly([
            'price' => $this->original_price
        ]);
    }
}
