<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('car_id');
            $table->unsignedBigInteger('pickup_id');
            $table->unsignedBigInteger('drop_id');
            $table->timestamp('pickup_time')->nullable();
            $table->timestamp('drop_time')->nullable();
            $table->timestamp('delivered_on')->nullable();
            $table->timestamp('returned_on')->nullable();
            $table->boolean('approved')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('bookings', function (Blueprint $table) {
            $table->foreign('user_id')
                  ->references('id')
                  ->on('users');

            $table->foreign('car_id')
                  ->references('id')
                  ->on('cars');

            $table->foreign('pickup_id')
                  ->references('id')
                  ->on('locations');

            $table->foreign('drop_id')
                  ->references('id')
                  ->on('locations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
