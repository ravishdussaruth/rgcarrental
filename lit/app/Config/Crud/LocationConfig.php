<?php

namespace Lit\Config\Crud;

use App\Models\Location;
use Ignite\Crud\Config\CrudConfig;
use Ignite\Crud\CrudIndex;
use Ignite\Crud\CrudShow;
use Lit\Http\Controllers\Crud\LocationController;

class LocationConfig extends CrudConfig
{
    /**
     * Model class.
     *
     * @var string
     */
    public $model = Location::class;

    /**
     * Controller class.
     *
     * @var string
     */
    public $controller = LocationController::class;

    /**
     * Model singular and plural name.
     *
     * @param Location|null location
     *
     * @return array
     */
    public function names(Location $location = null)
    {
        return [
            'singular' => 'Location',
            'plural'   => 'Locations',
        ];
    }

    /**
     * Get crud route prefix.
     *
     * @return string
     */
    public function routePrefix()
    {
        return 'locations';
    }

    /**
     * Build index page.
     *
     * @param \Ignite\Crud\CrudIndex $page
     *
     * @return void
     */
    public function index(CrudIndex $page)
    {
        $page->table(function ($table) {
            $table->col('Location')
                  ->value('{location_name}')
                  ->sortBy('title');
        })->search('title');
    }

    /**
     * Setup show page.
     *
     * @param \Ignite\Crud\CrudShow $page
     *
     * @return void
     */
    public function show(CrudShow $page)
    {
        $page->card(function ($form) {
            $form->input('location_name')
                 ->title('Location');
        });
    }
}
