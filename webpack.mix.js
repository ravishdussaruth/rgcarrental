const mix = require('laravel-mix');
const path = require('path');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.webpackConfig({
    output: {
        chunkFilename: mix.inProduction() ? 'js/chunks/[name].[chunkhash].js' : 'js/chunks/[name].js'
    },
    resolve: {
        alias: {
            '@': path.resolve(__dirname, 'resources/js/'),
            '@lit-js': path.resolve(__dirname, 'node_modules/litstack/resources/js/'),
            '@lit-sass': path.resolve(__dirname, 'node_modules/litstack/resources/sass/'),
            vue$: mix.inProduction() ? 'vue/dist/vue.min.js' : 'vue/dist/vue'
        }
    },

});

mix.copy('resources/js/assets', 'public/images');

mix.js('lit/resources/js/app.js', 'public/lit/js')
    .vue({version: 2})
    .extract(['vue']);

mix.sass('lit/resources/sass/app.scss', 'public/lit/css')
    .css('resources/css/lit.css', 'public/css')
    .css('resources/css/app.css', 'public/css')
    .js('resources/js/app.js', 'public/js')
    .copyDirectory('resources/assets', 'public')
    .minify('public/lit/css/app.css')
    .vue({version: 2})
    .extract(['vue']);


if (mix.inProduction()) {
    mix.version();
}
