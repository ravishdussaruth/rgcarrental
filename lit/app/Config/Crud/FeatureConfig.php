<?php

namespace Lit\Config\Crud;

use Ignite\Crud\CrudShow;
use Ignite\Crud\CrudIndex;
use Ignite\Crud\Config\CrudConfig;
use Illuminate\Support\Str;

use App\Models\Feature;
use Lit\Http\Controllers\Crud\FeatureController;

class FeatureConfig extends CrudConfig
{
    /**
     * Model class.
     *
     * @var string
     */
    public $model = Feature::class;

    /**
     * Controller class.
     *
     * @var string
     */
    public $controller = FeatureController::class;

    /**
     * Model singular and plural name.
     *
     * @param Feature|null feature
     *
     * @return array
     */
    public function names(Feature $feature = null)
    {
        return [
            'singular' => 'Feature',
            'plural'   => 'Features',
        ];
    }

    /**
     * Get crud route prefix.
     *
     * @return string
     */
    public function routePrefix()
    {
        return 'features';
    }

    /**
     * Build index page.
     *
     * @param \Ignite\Crud\CrudIndex $page
     *
     * @return void
     */
    public function index(CrudIndex $page)
    {
        $page->table(function ($table) {
            $table->col('Name')->value('{name}')->sortBy('title');
        })->search('name');
    }

    /**
     * Setup show page.
     *
     * @param \Ignite\Crud\CrudShow $page
     *
     * @return void
     */
    public function show(CrudShow $page)
    {
        $page->card(function ($form) {
            $form->input('name');
        });
    }
}
