@extends('partials.app', ['position' => 'position:relative'])

@section('title')
	RgCarRental - Our Goal Is Your Satisfaction
@endsection

@section('header')
	{!! SEO::generate() !!}
@endsection

@section('app')
	@include('partials.hero')

	<page-section id="services"
				  title="Services">
		<services :services="{{ $services }}"></services>
	</page-section>

	<page-section id="destinations"
				  title="Destinations"
				  description="Discover some of our trips made with wonderful people"
				  styles="background: #f9f9f9;padding: 50px 0 90px !important;">
		<destinations class="mt-2" :destinations="{{ $destinations }}"></destinations>
	</page-section>

	<page-section title="Our Cars"
				  id="cars"
				  description="Book our cars and enjoy a comfortable and hassle free drive with our new brand cars."
				  style="background: #f9f9f9;">
		<list :cars="{{ $cars }}"></list>
	</page-section>
@endsection
