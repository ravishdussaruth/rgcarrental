<?php

namespace App\Enums\Vehicle;

use Illuminate\Support\Collection;

class Transmissions extends \App\Enums\Enum
{
    /**
     * All car.
     *
     * @var int
     */
    const ALL = 1;

    /**
     * The automatic transmission.
     *
     * @var int
     */
    const AUTOMATIC = 2;

    /**
     * The manual transmission.
     *
     * @var int
     */
    const MANUAL = 3;

    /**
     * Transmission available.
     *
     * @return array
     */
    public static function transmissions(): array
    {
        return [
            self::AUTOMATIC,
            self::MANUAL
        ];
    }
}
