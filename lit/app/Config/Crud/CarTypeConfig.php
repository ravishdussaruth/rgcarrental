<?php

namespace Lit\Config\Crud;

use Ignite\Crud\CrudShow;
use Ignite\Crud\CrudIndex;
use Ignite\Crud\Config\CrudConfig;
use Illuminate\Support\Str;

use App\Models\CarType;
use Lit\Http\Controllers\Crud\CarTypeController;

class CarTypeConfig extends CrudConfig
{
    /**
     * Model class.
     *
     * @var string
     */
    public $model = CarType::class;

    /**
     * Controller class.
     *
     * @var string
     */
    public $controller = CarTypeController::class;

    /**
     * Model singular and plural name.
     *
     * @param CarType|null carType
     *
     * @return array
     */
    public function names(CarType $carType = null)
    {
        return [
            'singular' => 'Car Type',
            'plural'   => 'Car Types',
        ];
    }

    /**
     * Get crud route prefix.
     *
     * @return string
     */
    public function routePrefix()
    {
        return 'car-types';
    }

    /**
     * Build index page.
     *
     * @param \Ignite\Crud\CrudIndex $page
     *
     * @return void
     */
    public function index(CrudIndex $page)
    {
        $page->table(function ($table) {
            $table->col('Name')->value('{name}')->sortBy('title');
        })->search('name');
    }

    /**
     * Setup show page.
     *
     * @param \Ignite\Crud\CrudShow $page
     *
     * @return void
     */
    public function show(CrudShow $page)
    {
        $page->card(function ($form) {
            $form->input('name');
        });
    }
}
