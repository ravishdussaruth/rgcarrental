@extends('partials.app')

@section('app')
    <div class="mt-5 container">
        @if($destinations->isNotEmpty())
            <destination-list :destinations="{{ $destinations }}"></destination-list>
        @else
            <div class="d-flex p-5 justify-content-center">
                <h3 class="text-danger" style="opacity: 0.4">Nothing Here!</h3>
            </div>
        @endif
    </div>
@endsection
