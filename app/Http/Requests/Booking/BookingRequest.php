<?php

namespace App\Http\Requests\Booking;

use App\Enums\BookingType;
use App\Enums\User\Titles;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class BookingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'car_id' => ['sometimes', Rule::exists('cars', 'id')],
            'drop_time' => ['date', 'date_format:Y-m-d H:i', 'sometimes', 'after:pickup_time'],
            'pickup_time' => ['date', 'date_format:Y-m-d H:i', 'sometimes', 'before:drop_time'],
            'pickup_id' => ['sometimes', Rule::exists('locations', 'id')],
            'drop_id' => ['sometimes', Rule::exists('locations', 'id')],
            'pickup_address' => ['nullable'],
            'drop_address' => ['nullable'],
            'remarks' => ['nullable'],
            'budget' => ['nullable'],
            'no_of_passenger' => ['sometimes'],
            'price' => ['nullable'],
            'discount_id' => ['nullable'],
            'user.title' => ['sometimes', Rule::in(Titles::getValues())],
            'user.first_name' => ['required'],
            'user.last_name' => ['required'],
            'user.email' => ['nullable', 'email'],
            'user.phone_number' => ['required'],
        ];
    }

    /**
     * The booking data.
     *
     * @return array
     */
    public function booking(): array
    {
        return $this->only([
            'car_id',
            'drop_time',
            'pickup_time',
            'pickup_id',
            'drop_id',
            'booking_type',
            'drop_address',
            'pickup_address',
            'no_of_passenger',
            'price',
            'budget',
            'remarks',
            'discount_id',
        ]);
    }

    /**
     * Will tell us if this booking is for car rental.
     *
     * @return bool
     */
    public function isRentType(): bool
    {
        return $this->booking_type === BookingType::RENT;
    }

    /**
     * Will tell us if this booking type is for tour.
     *
     * @return bool
     */
    public function isTourType(): bool
    {
        return $this->booking_type === BookingType::TOUR;
    }

    /**
     * Will tell us if there is a discount associated to this booking.
     *
     * @return bool
     */
    public function hasDiscount(): bool
    {
        return !is_null($this->discount_id);
    }
}
