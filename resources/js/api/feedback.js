import axios from 'axios';

/**
 * Submit feedback.
 *
 * @param {Object} data
 *
 * @return {Promise<AxiosResponse<any>>}
 */
export const submit = (data) => axios.post('/submit-feedback', data);
