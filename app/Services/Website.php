<?php

namespace App\Services;

use App\Contracts\Home;
use Ignite\Support\Facades\Form;
use Illuminate\Support\Collection;
use Lit\Models\User;

class Website implements Home
{
    /**
     * The form model.
     *
     * @var Form $home
     */
    protected $home;

    /**
     * Website constructor.
     */
    public function __construct()
    {
        $this->boot();
    }

    /**
     * Retrieve the form model.
     */
    protected function boot()
    {
        $this->home = Form::load('pages', 'home');
    }

    /**
     * Will tell us if home config is available.
     *
     * @return bool
     */
    protected function hasHomeConfig(): bool
    {
        return !is_null($this->home);
    }

    /**
     * The cover image.
     *
     * @return ?string
     */
    public function cover(): ?string
    {
        return optional($this->home->cover)->original_url;
    }

    /**
     * The website logo.
     *
     * @return ?string
     */
    public function logo(): ?string
    {
        return optional($this->home->logo)->original_url;
    }

    /**
     * The website slogan.
     *
     * @return ?string
     */
    public function slogan(): ?string
    {
        return optional($this->home)->slogan;
    }

    /**
     * RGCarRental admins.
     *
     * @return Collection
     */
    public function admins(): Collection
    {
        if (!isset($this->home->contact_user)) {
            return User::all();
        }

        return User::where('id', $this->home->contact_user)
                   ->get();
    }

    /**
     * The seo meta.
     *
     * @return array|false|string[]
     */
    public function seo(): array
    {
        return $this->hasHomeConfig() ? explode(',', $this->home->meta) : [];
    }

    /**
     * The contact numbers.
     *
     * @return Collection
     */
    public function contactNumbers(): Collection
    {
        return $this->home->contact_numbers->pluck('contact_number');
    }

    /**
     * The company email address.
     *
     * @return string
     */
    public function email(): string
    {
        return $this->home->email_address;
    }

    /**
     * The company address.
     *
     * @return string
     */
    public function address(): string
    {
        return $this->home->address;
    }

    /**
     * Retrieve params from home.
     *
     * @param $name
     *
     * @return mixed
     */
    public function __get($name)
    {
        return $this->home->{$name};
    }
}
