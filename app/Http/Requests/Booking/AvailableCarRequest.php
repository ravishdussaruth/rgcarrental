<?php

namespace App\Http\Requests\Booking;

use App\Enums\Vehicle\Transmissions;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AvailableCarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'drop_time'    => ['date', 'date_format:Y-m-d H:i', 'required', 'after:pickup_time'],
            'pickup_time'  => ['date', 'date_format:Y-m-d H:i', 'required', 'before:drop_time'],
            'transmission' => ['sometimes', Rule::in(Transmissions::all()->pluck('id'))]
        ];
    }

    /**
     * Retrieve the transmissions.
     *
     * @return array
     */
    public function transmissions(): array
    {
        return $this->allCar() ? Transmissions::transmissions() : [$this->car_type];
    }

    /**
     * Will tell us if need to filter in all car types.
     *
     * @return bool
     */
    public function allCar(): bool
    {
        return !$this->transmissions || intval($this->transmissions) === Transmissions::ALL;
    }
}
