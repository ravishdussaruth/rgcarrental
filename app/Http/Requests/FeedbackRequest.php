<?php

namespace App\Http\Requests;

use App\Models\Booking;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FeedbackRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'booking'  => ['required', Rule::exists('bookings', 'reference')],
            'ratings'  => ['required'],
            'feedback' => ['nullable']
        ];
    }

    /**
     * Retrieve the booking.
     *
     * @return Booking
     */
    public function booking(): Booking
    {
        return Booking::whereReference($this->booking)->first();
    }

    /**
     * The feedback data.
     *
     * @return array
     */
    public function data(): array
    {
        return $this->only([
            'ratings',
            'feedback'
        ]);
    }
}
