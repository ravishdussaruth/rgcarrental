@extends('emails._partial')

@section('content')
    <img src="{{  asset('images/success.svg') }}" width="30%"/>

    <p class="font-weight-bold" style="font-size: 24px;">Your booking has been placed. An admin will soon contact you to
        confirm your booking.</p>

    <span class="font-weight-normal" style="font-size: 16px;">Find your booking info below</span>

    <span class="mt-1">Status: <span class="font-weight-bold">Pending</span></span>

    <table class="table mt-3">
        <thead>
        <tr>
            <th scope="col"></th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Reference</td>
            <td>{{ $booking->reference }}</td>
        </tr>

        <tr>
            <td>Pick Up Address</td>
            <td>{{ $booking->pickup_address }}</td>
        </tr>

        <tr>
            <td>Drop Address</td>
            <td>{{ $booking->drop_address }}</td>
        </tr>

        <tr>
            <td>Pick Up Date</td>
            <td>{{ $booking->pickup_time }}</td>
        </tr>
        </tbody>
    </table>
@endsection
