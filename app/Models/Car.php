<?php

namespace App\Models;

use App\Enums\Vehicle\RateTypes;
use App\Enums\Vehicle\Transmissions;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Ignite\Crud\Models\Traits\HasMedia;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Spatie\MediaLibrary\HasMedia as HasMediaContract;
use Spatie\Sitemap\Contracts\Sitemapable;
use Spatie\Sitemap\Tags\Url;

class Car extends Model implements HasMediaContract, Sitemapable
{
    use HasMedia, HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'transmission',
        'car_cc',
        'color',
        'ratings',
        'price',
        'rate_type',
        'description',
        'features',
        'max_seats',
        'slug',
        'car_type_id'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['images', 'thumbnail', 'car_transmission', 'car_rate'];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['media'];

    /**
     * Perform any actions required before the model boots.
     *
     * @return void
     */
    protected static function booting()
    {
        static::creating(function (Model $model) {
            $model->slug = Str::slug($model->name);
        });

        static::updating(function (Model $model) {
            $model->slug = Str::slug($model->name);
        });
    }

    public function toSitemapTag(): Url | string | array
    {
        return route('car.show', $this);
    }

    /**
     * Images attribute.
     *
     * @return Attribute
     */
    public function images(): Attribute
    {
        return Attribute::get(fn() => $this->getMedia('images'));
    }

    /**
     * Retrieve the thumbnail.
     *
     * @return Attribute
     */
    public function thumbnail(): Attribute
    {
        return Attribute::get(fn() => $this->media->first());
    }

    /**
     * The car transmission.
     *
     * @return Attribute
     */
    public function carTransmission(): Attribute
    {
        return Attribute::get(fn($value, $attributes) => Transmissions::getDescription(intval($attributes['transmission'] ?? [])));
    }

    /**
     * The car rate.
     *
     * @return Attribute
     */
    public function carRate(): Attribute
    {
        return Attribute::get(fn($value, $attributes) => RateTypes::getDescription(intval($attributes['rate_type'] ?? [])));
    }

    /**
     * The bookings for this car.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }

    /**
     * The features for this car.
     *
     * @return BelongsToMany
     */
    public function features(): BelongsToMany
    {
        return $this->belongsToMany(Feature::class, 'car_feature')
                    ->withTimestamps();
    }
}
