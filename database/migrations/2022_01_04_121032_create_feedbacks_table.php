<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeedbacksTable extends Migration
{
    use \App\Support\Migrationable;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedbacks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('booking_id');
            $table->text('feedback')
                  ->nullable();
            $table->integer('ratings')
                  ->nullable();
            $table->boolean('display_on_website')
                  ->default(false);
            $table->timestamps();
            $table->softDeletes();
        });

        if ($this->isNotTesting()) {
            Schema::table('feedbacks', function (Blueprint $table) {
                $table->foreign('booking_id')
                      ->references('id')
                      ->on('bookings');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ($this->isNotTesting()) {
            Schema::table('feedbacks', function (Blueprint $table) {
                $table->dropForeign(['booking_id']);
            });
        }

        Schema::dropIfExists('feedbacks');
    }
}
