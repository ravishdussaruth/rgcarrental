<?php

namespace App\Notifications\Booking\Taxi;

use Illuminate\Notifications\Messages\MailMessage;
use App\Notifications\Booking\Concerns\BookingNotification;

class NotifyOwnerAboutNewBooking extends BookingNotification
{
    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject($this->subject())
            ->line('Hello a new taxi booking has been made on the system')
            ->action('View Booking', route('lit.crud.taxi_booking.show', $this->booking));
    }

    /**
     * The email subject.
     *
     * @return string
     */
    protected function subject(): string
    {
        return 'New Taxi Booking';
    }
}
