<?php

namespace Lit\Config\Crud;

use App\Models\Discount;
use App\Models\PromotionalOffer;
use Ignite\Crud\Config\CrudConfig;
use Ignite\Crud\CrudIndex;
use Ignite\Crud\CrudShow;
use Illuminate\Database\Eloquent\Builder;
use Lit\Http\Controllers\Crud\PromotionalOfferController;

class PromotionalOfferConfig extends CrudConfig
{
    /**
     * Model class.
     *
     * @var string
     */
    public $model = PromotionalOffer::class;

    /**
     * Controller class.
     *
     * @var string
     */
    public $controller = PromotionalOfferController::class;

    /**
     * Model singular and plural name.
     *
     * @param PromotionalOffer|null promotionalOffer
     * @return array
     */
    public function names(PromotionalOffer $promotionalOffer = null)
    {
        return [
            'singular' => 'Promotional Offer',
            'plural'   => 'Promotional Offers',
        ];
    }

    /**
     * Get crud route prefix.
     *
     * @return string
     */
    public function routePrefix()
    {
        return 'promotional-offers';
    }

    /**
     * Build index page.
     *
     * @param  \Ignite\Crud\CrudIndex $page
     * @return void
     */
    public function index(CrudIndex $page)
    {
        $page->table(function ($table) {
            $table->col('Discount Code')->value('{code}');
            $table->col('Discount')->value('{discount.name}');
            $table->col('Start On')->value('{start_on}');
            $table->col('End On')->value('{end_on}');
            $table->col('Active')->value('is_active', [
                true  => '<div class="badge badge-success">Active</div>',
                false => '<div class="badge badge-danger">Not Active</div>',
            ])->sortBy('is_active');
        })
            ->query(fn (Builder $query) => $query->with('discount'))
            ->search('title');
    }

    /**
     * Setup show page.
     *
     * @param  \Ignite\Crud\CrudShow $page
     * @return void
     */
    public function show(CrudShow $page)
    {
        $page->card(function ($form) {
            $form->boolean('is_active')
                ->title('Enable promotional offers');

            $form->select('discount_id')
                ->title('Discount')
                ->options(
                    Discount::all()->mapWithKeys(fn (Discount $discount) => [$discount->id => $discount->name])->toArray()
                )
                ->width(1 / 2);

            $form->input('code')
                ->title('Discount code')
                ->width(1 / 2);

            $form->datetime('start_on')
                ->title('Start On');

            $form->datetime('end_on')
                ->title('End On');
        });
    }
}
