<?php

namespace App\Models\Concerns;

use App\Enums\BookingType;
use App\Contracts\Booking\Notification;

trait HasNotifications
{
    /**
     * The notification service to send emails about booking type.
     *
     * @return ?Notification
     */
    protected function notificationService(): ?Notification
    {
        return BookingType::notificationFor($this->booking_type);
    }

    /**
     * Notify user about the changes in booking.
     *
     * @return void
     */
    public function notifyUserAboutBookingApprovalChanges(): void
    {
        if (!isset($this->booking_type)) {
            return;
        }

        if ($this->isConfirmed()) {
            $this->notificationService()->notifyConfirmed($this);
            return;
        }

        $this->notificationService()->notifyCanceled($this);
    }

    /**
     * Notify owner about new bookings.
     *
     * @return void
     */
    public function notifyOwnerAboutBookingMade(): void
    {
        $this->notificationService()->notifyOwnerAboutBooking($this);
    }

    /**
     * Notify user that their booking has been completed and ask for a feedback.
     *
     * @return void
     */
    public function notifyBookingHasBeenCompleted(): void
    {
        $this->notificationService()->notifyCompleted($this);
    }

    /**
     * Notify user about the creation of their booking.
     *
     * @return void
     */
    public function notifyUserAboutBooking(): void
    {
        $this->notificationService()->notifyCreated($this);
    }
}
