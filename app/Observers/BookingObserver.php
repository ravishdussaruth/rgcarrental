<?php

namespace App\Observers;

use App\Models\Booking;

class BookingObserver
{
    /**
     * User has created a new booking, and notifying admins about that.
     *
     * @param Booking $booking
     *
     * @return void
     */
    public function created(Booking $booking)
    {
        $booking->notifyOwnerAboutBookingMade();
        $booking->notifyUserAboutBooking();
    }

    /**
     * When a booking have been updated.
     *
     * @param Booking $booking
     *
     * @return void
     */
    public function updated(Booking $booking): void
    {
        if ($booking->originalPriceJustChanged()) {
            $booking->resetPriceToOriginalPrice();
        }

        if ($booking->discountJustChanged()) {
            $booking->applyDiscount();
        }

        if ($booking->approvedJustChanged()) {
            $booking->notifyUserAboutBookingApprovalChanges();
        }

        if ($booking->bookingCompleted()) {
            $booking->notifyBookingHasBeenCompleted();
        }
    }
}
