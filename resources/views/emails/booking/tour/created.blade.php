@extends('emails._partial')

@section('content')
    <img src="{{  asset('images/success.svg') }}" width="30%"/>

    <p class="font-weight-bold" style="font-size: 24px;">Your booking has been placed. An admin will soon contact you to
        confirm it.</p>

    <span class="font-weight-normal" style="font-size: 16px;">Find your booking info below</span>

    <table class="table mt-3" style="width: 50%;">
        <thead>
        <tr>
            <th scope="col"></th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Reference</td>
            <td>{{ $booking->reference }}</td>
        </tr>

        <tr>
            <td>Regions</td>
            <td>{{ $booking->booking_regions }}</td>
        </tr>

        <tr>
            <td>Number of passenger</td>
            <td>{{ $booking->no_of_passenger }}</td>
        </tr>

        <tr>
            <td>Pick Up Address</td>
            <td>{{ $booking->pickup_address }}</td>
        </tr>

        <tr>
            <td>Drop Address</td>
            <td>{{ $booking->drop_address }}</td>
        </tr>

        <tr>
            <td>Pick Up Date Time</td>
            <td>{{ $booking->pickup_time }}</td>
        </tr>

        <tr>
            <td>Drop Date Time</td>
            <td>{{ $booking->drop_time }}</td>
        </tr>
        </tbody>
    </table>
@endsection
