<?php

namespace App\Http\Controllers\Page;

use App\Contracts\Home;
use App\Http\Controllers\Concerns\HasSeo;
use App\Http\Controllers\Controller;
use Artesaos\SEOTools\Facades\SEOMeta;
use Facades\Artesaos\SEOTools\SEOTools;
use Illuminate\Http\Request;

class TourController extends Controller
{
    use HasSeo;

    public function __invoke(Home $home)
    {
        $this->render($home);

        SEOMeta::setDescription('Book a tour online with us, available 24/7 anywhere in Mauritius. Select your destinations from our booking app, or call us for any other information.');

        return view('tour');
    }
}
