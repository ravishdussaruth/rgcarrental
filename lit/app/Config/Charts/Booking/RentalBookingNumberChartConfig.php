<?php

namespace Lit\Config\Charts\Booking;

use App\Models\Booking;
use Ignite\Chart\Chart;
use App\Enums\BookingType;
use Ignite\Chart\Config\NumberChartConfig;
use Illuminate\Database\Eloquent\Builder;

class RentalBookingNumberChartConfig extends NumberChartConfig
{
    /**
     * The model class of the chart.
     *
     * @var string
     */
    public $model = Booking::class;

    /**
     * Chart title.
     *
     * @return string
     */
    public function title(): string
    {
        return 'Money in';
    }

    /**
     * Mount.
     *
     * @param Chart $chart
     *
     * @return void
     */
    public function mount(Chart $chart)
    {
        $chart->currency('Rs')
              ->format('0.0');
    }

    /**
     * Calculate value.
     *
     * @param Builder $query
     *
     * @return int
     */
    public function value($query)
    {
        return $this->sum(
            $query->with('bookingPayment')
                  ->whereHas('bookingPayment', fn($query) => $query->where('completed', true))
                  ->where('booking_type', BookingType::RENT)
                  ->where('approved', true),
            'price');
    }
}
