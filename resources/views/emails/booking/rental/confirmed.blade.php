@extends('emails._partial')

@section('content')
    <img src="{{  asset('images/success.svg') }}" width="30%"/>

    <span class="font-weight-bold" style="font-size: 24px;">Yaaay! Your car booking has been confirmed</span>

    <span class="font-weight-normal" style="font-size: 16px;">Your Booking Info</span>

    <table class="table">
        <thead>
        <tr>
            <th scope="col"></th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Reference</td>
            <td>{{ $booking->reference }}</td>
        </tr>

        <tr>
            <td>Pick Up Location</td>
            <td>{{ $booking->pickupLocation->location_name }}</td>
        </tr>

        <tr>
            <td>Drop Location</td>
            <td>{{ $booking->dropLocation->location_name }}</td>
        </tr>

        <tr>
            <td>Pick Up Date</td>
            <td>{{ $booking->pickup_time }}</td>
        </tr>

        <tr>
            <td>Drop Date</td>
            <td>{{ $booking->drop_time }}</td>
        </tr>

        <tr>
            <td>Comment</td>
            <td>{{ $booking->rejected_comment }}</td>
        </tr>
        </tbody>
    </table>
@endsection
