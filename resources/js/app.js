import Vue from 'vue';

import { BootstrapVue } from 'bootstrap-vue';
import VueSweetalert2 from 'vue-sweetalert2';

import 'sweetalert2/dist/sweetalert2.min.css';

Vue.use(VueSweetalert2);
Vue.use(BootstrapVue);

require('./global-components');

new Vue({
    el: '#app'
});