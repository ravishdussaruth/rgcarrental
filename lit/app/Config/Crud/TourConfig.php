<?php

namespace Lit\Config\Crud;

use Ignite\Crud\CrudShow;
use Ignite\Crud\CrudIndex;
use Ignite\Crud\Config\CrudConfig;
use Illuminate\Support\Str;

use App\Models\Tour;
use Lit\Http\Controllers\Crud\TourController;

class TourConfig extends CrudConfig
{
    /**
     * Model class.
     *
     * @var string
     */
    public $model = Tour::class;

    /**
     * Controller class.
     *
     * @var string
     */
    public $controller = TourController::class;

    /**
     * Model singular and plural name.
     *
     * @param Tour|null tour
     * @return array
     */
    public function names(Tour $tour = null)
    {
        return [
            'singular' => 'Tour',
            'plural'   => 'Tours',
        ];
    }

    /**
     * Get crud route prefix.
     *
     * @return string
     */
    public function routePrefix()
    {
        return 'tours';
    }

    /**
     * Build index page.
     *
     * @param \Ignite\Crud\CrudIndex $page
     * @return void
     */
    public function index(CrudIndex $page)
    {
        $page->table(function ($table) {

            $table->col('Title')->value('{title}')->sortBy('title');

        })->search('title');  
    }

    /**
     * Setup show page.
     *
     * @param \Ignite\Crud\CrudShow $page
     * @return void
     */
    public function show(CrudShow $page)
    {
        $page->card(function($form) {

            $form->input('title');
            
        });
    }
}
