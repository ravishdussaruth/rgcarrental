@extends('partials.app-header')

@section('app')
    <div class="m-auto w-50" style="padding: 70px;">
        <h4 class="font-weight-bold text-center">We Value Your Opinion</h4>

        <p class="text-center font-weight-normal">Please take a moment to leave a feedback so that we can make our
            service even better.</p>

        <feedback booking="{{ request()->get('booking') }}"></feedback>
    </div>
@endsection
