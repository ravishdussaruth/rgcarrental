<?php

namespace App\Notifications\Booking\Taxi;

use App\Notifications\Booking\Concerns\BookingNotification;

class NotifyUserBookingCanceled extends BookingNotification
{
    /**
     * The view path.
     *
     * @return string
     */
    protected function view(): string
    {
        return 'emails.booking.taxi.canceled';
    }

    /**
     * The email subject.
     *
     * @return string
     */
    protected function subject(): string
    {
        return 'Taxi Booking Canceled';
    }
}
