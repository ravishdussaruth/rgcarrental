<?php

namespace App\Notifications\Booking\Concerns;

use App\Models\Booking;
use App\Support\Makable;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

abstract class BookingNotification extends Notification implements ShouldQueue
{
    use Queueable, Makable;

    /**
     * The user`s booking.
     *
     * @var Booking $booking
     */
    protected Booking $booking;

    /**
     * Create a new notification instance.
     *
     * @param Booking $booking
     *
     * @return void
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Create a new resource instance.
     *
     * @param mixed ...$parameters
     *
     * @return static
     */
    public static function make(...$parameters)
    {
        return new static(...$parameters);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject($this->subject())
            ->view($this->view(), $this->params());
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    /**
     * The view path.
     *
     * @return string
     */
    protected function view(): string
    {
        return 'emails.booking.canceled';
    }

    /**
     * The params for the notification.
     *
     * @return array
     */
    protected function params(): array
    {
        return [
            'booking' => $this->booking,
            'user'    => $this->booking->user
        ];
    }

    /**
     * The email subject.
     *
     * @return string
     */
    abstract protected function subject(): string;
}
