<?php

namespace Database\Factories;

use App\Models\Car;
use App\Enums\Vehicle\Transmissions;
use Illuminate\Database\Eloquent\Factories\Factory;

class CarFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Car::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'         => $this->faker->name,
            'transmission' => 2,
            'car_cc'       => 1200,
            'color'        => 'Gray',
            'ratings'      => 4,
            'price'        => 1200,
            'description'  => 'Nice Car',
            'features'     => '',
            'max_seats'    => 5,
            'rate_type'    => 1
        ];
    }

    /**
     * Indicate that the car is a manual car.
     *
     * @return Factory
     */
    public function manual()
    {
        return $this->state(fn() => [
            'transmission' => Transmissions::MANUAL
        ]);
    }

    /**
     * Indicate that the car is an automatic car.
     *
     * @return Factory
     */
    public function automatic()
    {
        return $this->state(fn() => [
            'transmission' => Transmissions::AUTOMATIC
        ]);
    }
}
