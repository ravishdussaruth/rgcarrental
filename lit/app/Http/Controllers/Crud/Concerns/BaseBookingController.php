<?php

namespace Lit\Http\Controllers\Crud\Concerns;

use Ignite\Crud\Controllers\CrudController;
use Illuminate\Contracts\Auth\Access\Authorizable;

abstract class BaseBookingController extends CrudController
{
    /**
     * Fill model on store.
     *
     * @param mixed $model
     *
     * @return void
     */
    public function fillOnStore($model)
    {
        $this->assignReferenceId($model);
        $this->assignBookingType($model);
    }

    /**
     * Authorize request for authenticated lit-user and permission operation.
     * Operations: create, read, update, delete.
     *
     * @param Authorizable $user
     * @param string       $operation
     * @param int          $id
     *
     * @return bool
     */
    public function authorize(Authorizable $user, string $operation, $id = null): bool
    {
        return true;
    }

    /**
     * Assign reference id to model.
     *
     * @param $model
     *
     * @return void
     */
    protected function assignReferenceId($model): void
    {
        $model->reference = generate_booking_id();
    }

    /**
     * Assign the booking type to this model.
     *
     * @param $model
     *
     * @return void
     */
    abstract protected function assignBookingType($model): void;
}
