@extends('partials.app', ['position' => 'position:relative'])

@section('title')
    RgCarRental - Book a tour.
@endsection

@section('header')
    {!! SEO::generate() !!}
@endsection

@section('app')
    <div class="pt-5 p-2 p-lg-5 mt-5">
        <div class="col-lg-6 col-sm-12 m-auto">
            <tour></tour>
        </div>
    </div>
@endsection
