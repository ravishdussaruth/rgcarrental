<?php

namespace App\Http\Controllers;

use App\Models\PromotionalOffer;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CouponDiscountController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->validate([
            'code' => ['required', Rule::exists('')],
        ]);

        $offer = PromotionalOffer::where('code', $request->code)
                                 ->where('is_active', true)
                                 ->first();

        return response()->json([
            'is_valid' => !is_null($offer),
            'value' => $offer->discount->coeff,
            'discount_id' => $offer->discount->uuid,
        ]);
    }
}
