<?php

namespace Lit\Config\Crud;

use App\Models\Service;
use Ignite\Crud\Config\CrudConfig;
use Ignite\Crud\CrudIndex;
use Ignite\Crud\CrudShow;
use Lit\Http\Controllers\Crud\ServiceController;

class ServiceConfig extends CrudConfig
{
    /**
     * Model class.
     *
     * @var string
     */
    public $model = Service::class;

    /**
     * Controller class.
     *
     * @var string
     */
    public $controller = ServiceController::class;

    /**
     * Form route prefix.
     *
     * @return string
     */
    public function routePrefix()
    {
        return "pages/services";
    }

    /**
     * Form singular name. This name will be displayed in the navigation.
     *
     * @return array
     */
    public function names()
    {
        return [
            'singular' => 'Service',
            'plural'   => 'Services'
        ];
    }

    /**
     * Build index page.
     *
     * @param \Ignite\Crud\CrudIndex $page
     *
     * @return void
     */
    public function index(CrudIndex $page)
    {
        $page->table(function ($table) {
            $table->image('Image')
                  ->src('{image.conversion_urls.sm}')
                  ->maxWidth('50px')
                  ->maxHeight('50px');

            $table->col('Name')
                  ->value('{name}')
                  ->sortBy('name');
        })->search('name');
    }

    /**
     * Setup form page.
     *
     * @param \Lit\Crud\CrudShow $page
     *
     * @return void
     */
    public function show(CrudShow $page)
    {
        $page->card(function ($form) {
            $form->image('image')
                 ->title('Photo')
                 ->hint('Upload Service Photo');

            $form->input('name')
                 ->creationRules('required')
                 ->rules('min:2', 'max:255')
                 ->title('Service Name');

            $form->textarea('description')
                 ->title('Description');
        });
    }
}
