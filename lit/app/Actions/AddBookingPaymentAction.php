<?php

namespace Lit\Actions;

use App\Models\Booking;
use App\Models\PaymentType;
use Ignite\Support\AttributeBag;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;
use Ignite\Page\Actions\ActionModal;

class AddBookingPaymentAction
{
    /**
     * Display the modal.
     *
     * @param ActionModal $modal
     */
    public function modal(ActionModal $modal)
    {
        $modal->form(function ($form) {
            $form->select('payment_type_id')
                 ->title('Payment Type')
                 ->options(
                     PaymentType::all()->mapWithKeys(fn(PaymentType $paymentType) => [$paymentType->id => $paymentType->name])->toArray()
                 )
                 ->hint('Select Payment Type.')
                 ->width(1 / 2);

            $form->boolean('completed')
                 ->title('Completed')
                 ->width(1 / 2);
        })
              ->confirmVariant('primary')
              ->confirmText('Add Payment')
              ->message('Do you want to add this payment?');;
    }

    /**
     * Run the action.
     *
     * @param Collection   $models
     * @param AttributeBag $attributes
     *
     * @return JsonResponse
     */
    public function run(Collection $models, AttributeBag $attributes)
    {
        /** @var Booking $model */
        $model = $models->first();

        $model->bookingPayment()
              ->delete();

        $model->bookingPayment()->create($attributes->toArray());

        return redirect($model->route());
    }
}
