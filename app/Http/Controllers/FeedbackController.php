<?php

namespace App\Http\Controllers;

use App\Models\Feedback;
use App\Http\Requests\FeedbackRequest;

class FeedbackController extends Controller
{
    /**
     * Save feedback for this booking.
     *
     * @param FeedbackRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(FeedbackRequest $request)
    {
        $feedback = $request->booking()
                            ->feedbacks()
                            ->create($request->data());

        return response()->json([
            'completed' => $feedback instanceof Feedback,
            'message'   => 'Thank you for taking the time to provide us with your feedback.'
        ]);
    }
}
