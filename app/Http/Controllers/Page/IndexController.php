<?php

namespace App\Http\Controllers\Page;

use App\Http\Controllers\Concerns\HasSeo;
use App\Models\Car;
use App\Models\Service;
use App\Contracts\Home;
use App\Models\Destination;
use App\Http\Controllers\Controller;
use App\Enums\User\Destination as Destinations;
use Artesaos\SEOTools\Facades\SEOMeta;
use Facades\Artesaos\SEOTools\SEOTools;

class IndexController extends Controller
{
    use HasSeo;

    /**
     * Retrieve home details.
     *
     * @param Home $home
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function __invoke(Home $home)
    {
        $this->render($home);

        return view('index', [
            'cars'         => Car::all()->toJson(),
            'services'     => Service::all()->toJson(),
            'regions'      => Destinations::toJson(),
            'destinations' => Destination::whereDisplayOnSite(true)->get()->toJson()
        ]);
    }
}
