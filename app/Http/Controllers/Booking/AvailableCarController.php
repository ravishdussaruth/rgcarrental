<?php

namespace App\Http\Controllers\Booking;

use App\Models\Car;
use App\Http\Controllers\Controller;
use App\Http\Resources\CarCollection;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Requests\Booking\AvailableCarRequest;
use Illuminate\View\View;

class AvailableCarController extends Controller
{
    /**
     * Look for available cars.
     *
     * @param AvailableCarRequest $request
     *
     * @return CarCollection
     */
    public function index(AvailableCarRequest $request)
    {
        return CarCollection::make(Car::with('bookings')
                                      ->whereDoesntHave('bookings', function (Builder $query) use ($request) {
                                          $query->whereNotBetween('pickup_time', [$request->pickup_time, $request->drop_time])
                                                ->where('approved', false);
                                      })
                                      ->whereIn('transmission', $request->transmissions())
                                      ->get());
    }

    /**
     * Look for available cars.
     *
     * @param AvailableCarRequest $request
     *
     * @return View
     */
    public function search(AvailableCarRequest $request)
    {
        $cars = Car::with('bookings')
                   ->whereDoesntHave('bookings', function (Builder $query) use ($request) {
                       $query->whereNotBetween('pickup_time', [$request->pickup_time, $request->drop_time])
                             ->where('approved', false);
                   })
                   ->whereIn('transmission', $request->transmissions())
                   ->get();

        return view('cars.index', compact('cars'));
    }
}
