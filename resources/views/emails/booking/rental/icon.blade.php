<div class="d-flex align-items-center justify-content-between" style="width: 35%;">
    <img src="{{ $source }}" style="width: 1rem;height: 1rem;"/>

    <span class="ml-2">{{ $text }}</span>
</div>
