<?php

namespace Lit\Config\Crud;

use App\Models\Destination;
use Ignite\Crud\Config\CrudConfig;
use Ignite\Crud\CrudIndex;
use Ignite\Crud\CrudShow;
use Lit\Http\Controllers\Crud\DestinationController;

class DestinationConfig extends CrudConfig
{
    /**
     * Model class.
     *
     * @var string
     */
    public $model = Destination::class;

    /**
     * Controller class.
     *
     * @var string
     */
    public $controller = DestinationController::class;

    /**
     * Model singular and plural name.
     *
     * @param Destination|null destination
     *
     * @return array
     */
    public function names(Destination $destination = null)
    {
        return [
            'singular' => 'Destination',
            'plural'   => 'Destinations',
        ];
    }

    /**
     * Get crud route prefix.
     *
     * @return string
     */
    public function routePrefix()
    {
        return 'destinations';
    }

    /**
     * Build index page.
     *
     * @param \Ignite\Crud\CrudIndex $page
     *
     * @return void
     */
    public function index(CrudIndex $page)
    {
        $page->table(function ($table) {
            $table->image('Image')
                  ->src('{image.conversion_urls.sm}')
                  ->maxWidth('50px')
                  ->maxHeight('50px');

            $table->col('Title')
                  ->value('{name}')
                  ->sortBy('title');

            $table->col('Category')
                  ->value('{destination_category}');
        })->search('title')
             ->filter([
                 'Category' => [
                     'north' => 'North',
                     'east'  => 'East',
                     'west'  => 'West',
                     'south' => 'South'
                 ]
             ]);
    }

    /**
     * Setup show page.
     *
     * @param \Ignite\Crud\CrudShow $page
     *
     * @return void
     */
    public function show(CrudShow $page)
    {
        $page->card(function (CrudShow $form) {
            $form->boolean('display_on_site')
                 ->title('Display on website ?')
                 ->width(1 / 2);

            $form->image('image')
                 ->title('Photo')
                 ->hint('Upload destination photo')
                 ->width(1 / 2);

            $form->input('name')
                 ->creationRules('required')
                 ->rules('min:2', 'max:255')
                 ->title('Destination Title')
                 ->width(1 / 2);

            $form->select('category')
                 ->title('Which Category ?')
                 ->options(\App\Enums\User\Destination::labels()->toArray())
                 ->width(1 / 2);

            $form->textarea('description')
                 ->creationRules('required')
                 ->title('Destination Description');
        });
    }
}
