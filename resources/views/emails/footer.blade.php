<footer style="position:relative;bottom: 0;background: #f9f9f9;padding: 30px;width: 100%;">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="single-footer-item">
                    <div class="footer-logo">
                        <img src="{{resolve(\App\Contracts\Home::class)->logo() }}"
                             alt="RgCarRental"
                             height="auto"
                             width="200px">
                        <p>
                            Our Goal is your Satisfaction
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="single-footer-item">
                    <h6 class="text-left text-capitalize font-weight-bolder text-display">link</h6>
                    <div class="single-footer-txt">
                        <p><a href="#">Home</a></p>
                        <p><a href="#">Tours</a></p>
                        <p><a href="#">Cars</a></p>
                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="single-footer-item">
                    <h6 class="text-left text-capitalize font-weight-bolder text-display">Popular Destination</h6>

                    <div class="single-footer-txt">
                        @foreach(\App\Enums\User\Destination::all() as $destination)
                            <p>
                                <a href="{{ route('app.destinations', ['category' => $destination['label']]) }}">{{ $destination['label'] }}</a>
                            </p>
                        @endforeach
                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="single-footer-item text-center">
                    <h6 class="text-left text-capitalize font-weight-bolder text-display">Contacts</h6>
                    <div class="single-footer-txt text-left">
                        <p>+(230) 6345-400</p>
                        <p>+(230) 5255-2210</p>
                        <p>+(230) 5757-3777</p>
                        <span class="text-display foot-email text-lowercase"><a
                                href="mailto:rgcarrentals@gmail.com">rgcarrentals@gmail.com</span>
                        <p>Old Grand Port</p>
                        <p>Mauritius</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
