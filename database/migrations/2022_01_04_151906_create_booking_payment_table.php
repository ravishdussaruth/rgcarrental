<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingPaymentTable extends Migration
{
    use \App\Support\Migrationable;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_payment', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('booking_id');
            $table->unsignedBigInteger('payment_type_id');
            $table->boolean('completed')
                  ->default(false);
            $table->timestamps();
            $table->softDeletes();
        });

        if ($this->isNotTesting()) {
            Schema::table('booking_payment', function (Blueprint $table) {
                $table->foreign('booking_id')
                      ->references('id')
                      ->on('bookings');

                $table->foreign('payment_type_id')
                      ->references('id')
                      ->on('payment_types');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ($this->isNotTesting()) {
            Schema::table('booking_payment', function (Blueprint $table) {
                $table->dropForeign(['booking_id']);
                $table->dropForeign(['payment_type_id']);
            });
        }
        Schema::dropIfExists('booking_payment');
    }
}
