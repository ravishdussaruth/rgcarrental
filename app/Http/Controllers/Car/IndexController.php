<?php

namespace App\Http\Controllers\Car;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\Car;
use Artesaos\SEOTools\Facades\SEOMeta;
use Carbon\CarbonPeriod;
use Facades\Artesaos\SEOTools\SEOTools;
use Illuminate\Support\Collection;

class IndexController extends Controller
{
    /**
     * Retrieve car details.
     *
     * @param Car $car
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(Car $car)
    {
        $bookings = $car->bookings()->where('pickup_time', '>', now())->get();

        $features = $car->features()->get();
        SEOTools::setTitle($car->name);
        SEOTools::setDescription($car->description);
        SEOTools::opengraph()->setUrl(request()->fullUrl());
        SEOTools::setCanonical(request()->fullUrl());
        SEOTools::opengraph()->addProperty('type', 'car');
        SEOMeta::setKeywords([...$features->pluck('name'), $car->name, $car->slug]);

        return view('cars.show', [
            'daysBooked' => $this->daysBooked($bookings),
            'car'        => $car,
            'media'      => $car->media->pluck('url', 'id')->toJson(),
            'features'   => $features,
            'otherCars'  => Car::where('id', '<>', $car->id)->get()
        ]);
    }

    /**
     * The days this car has been booked.
     *
     * @param Collection $bookings
     *
     * @return Collection
     */
    protected function daysBooked(Collection $bookings)
    {
        return $bookings->map(fn(Booking $booking) => collect(CarbonPeriod::create($booking->pickup_time, $booking->drop_time))->map(fn($date) => $date->format('Y-m-d')))
                        ->flatten();
    }
}
