<?php

namespace App\Contracts\PositionStack;

interface Query
{
    /**
     * Use this parameter.
     *
     * @param string $param
     * @param        $value
     *
     * @return self
     */
    public function withParam(string $param, $value): self;

    /**
     * All the params list.
     *
     * @return array
     */
    public function parameters(): array;

    /**
     * Run the request.
     *
     * @return mixed
     */
    public function get();
}
