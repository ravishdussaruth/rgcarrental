<div class="div-container">
  <div class="h-100 w-100 img-container" style="filter: brightness(0.9) grayscale(0.5) brightness(0.8) !important">
  </div>
  
  <div class="container">
    <div class="centered-text text-left d-flex flex-lg-row flex-column  text-white justify-content-lg-between justify-content-center align-items-center">
      <div class="d-flex flex-column col-lg-7 col-12">
        <h1 class="text-capitalize text-white font-weight-bold">
          Looking to save more on your next car rental ?
        </h1>
        
        <p style="font-size: 16px" class="text-white w-75 text-justify">
          You prefer to drive and explore the island on your own at an
          affordable price, then book your car now.
        </p>
        
        <img class="fade-enter-active" src="/images/car_cover.png"/>
        
        <div class="d-flex col-12 align-items-center">
          <a v-b-tooltip.hover title="Book a taxi"
             class="col-lg-4 col-5 d-flex align-items-center justify-content-around btn-color-secondary btn"
             href="{{ route('taxi.booking') }}">
                        <span class="text-black font-weight-bold text-uppercase">
                            Book a
                        </span>
            
            <img src="/images/taxi.png" width="75" height="56"/>
          </a>
          
          <a v-b-tooltip.hover title="Book a tour"
             class="col-lg-4 col-5 d-flex align-items-center justify-content-around btn-color-secondary btn ml-2 col-sm-12"
             href="{{ route('tour.booking') }}">
                        <span class="text-black font-weight-bold text-uppercase">
                            Book a
                        </span>
            
            <img src="/images/road-trip.png" width="75" height="56"/>
          </a>
        </div>
      </div>
      
      <search-car class="bg-white p-1 col-md-5 col-11 mt-5 mr-4 mt-lg-0 col-lg-4 shadow"/>
    </div>
  </div>
</div>
