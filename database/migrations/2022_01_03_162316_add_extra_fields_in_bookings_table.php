<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExtraFieldsInBookingsTable extends Migration
{
    use \App\Support\Migrationable;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->decimal('original_price')
                  ->default(0);

            $table->text('remarks')
                  ->nullable();

            $table->integer('budget')
                  ->nullable();

            $table->integer('currency')
                  ->default(\App\Enums\Currency::MUR);

            $table->unsignedBigInteger('discount_id')
                  ->nullable();
        });

        if ($this->isNotTesting()) {
            Schema::table('bookings', function (Blueprint $table) {
                $table->foreign('discount_id')
                      ->references('id')
                      ->on('discounts');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ($this->isNotTesting()) {
            Schema::table('bookings', function (Blueprint $table) {
                $table->dropForeign(['discount_id']);
            });
        }

        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn(['discount_id', 'remarks', 'budget', 'currency']);
        });
    }
}
