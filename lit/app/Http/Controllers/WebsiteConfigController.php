<?php

namespace Lit\Http\Controllers;

use Ignite\Crud\Controllers\FormController;
use Ignite\Page\Page;

class WebsiteConfigController extends FormController
{
    /**
     * Get welcome page.
     *
     * @return \Ignite\Page\Page
     */
    public function __invoke()
    {
        $page = new Page();

        $page->title('Website Configuration');

        $page->component('website-configuration');

        return $page;
    }
}
