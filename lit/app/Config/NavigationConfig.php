<?php

namespace Lit\Config;

use Lit\Config\Crud\BookingConfig;
use Ignite\Application\Navigation\Config;
use Lit\Config\Crud\CarTypeConfig;
use Lit\Config\Crud\DiscountConfig;
use Lit\Config\Crud\FeatureConfig;
use Lit\Config\Crud\FeedbackConfig;
use Lit\Config\Crud\PaymentTypeConfig;
use Lit\Config\Crud\RentDiscountConfig;
use Lit\Config\Crud\TourBookingConfig;
use Lit\Config\User\UserConfig;
use Lit\Config\Crud\CarConfig;
use Lit\Config\Crud\DestinationConfig;
use Lit\Config\Crud\LocationConfig;
use Lit\Config\Crud\ServiceConfig;
use Lit\Config\Crud\TaxiBookingConfig;
use Lit\Config\Form\Pages\HomeConfig;
use Ignite\Application\Navigation\Navigation;
use Lit\Config\Crud\PromotionalOfferConfig;

class NavigationConfig extends Config
{
    /**
     * Topbar navigation entries.
     *
     * @param \Ignite\Application\Navigation\Navigation $nav
     *
     * @return void
     */
    public function topbar(Navigation $nav)
    {
        $nav->section([
            $nav->preset('profile'),
        ]);

        $nav->section([
            $nav->title(__lit('navigation.user_administration')),

            $nav->preset('permissions'),
        ]);
    }

    /**
     * Main navigation entries.
     *
     * @param \Ignite\Application\Navigation\Navigation $nav
     *
     * @return void
     */
    public function main(Navigation $nav)
    {
        $nav->section([
            $nav->title('Admin'),
            $nav->preset(UserConfig::class, ['icon' => fa('users')]),
        ]);

        $nav->section([
            $nav->title('Configuration'),

            $nav->preset(HomeConfig::class, ['icon' => fa('wrench')]),
        ]);

        $nav->section([
            $nav->title('Website'),

            $nav->preset(FeatureConfig::class, ['icon' => fa('wrench')]),
            $nav->preset(CarTypeConfig::class, ['icon' => fa('question-mark')]),
            $nav->preset(CarConfig::class, ['icon' => fa('car')]),
            $nav->preset(ServiceConfig::class, ['icon' => fa('taxi')]),
            $nav->preset(LocationConfig::class, ['icon' => fa('map-marker-alt')]),
            $nav->preset(DestinationConfig::class, ['icon' => fa('map')]),
        ]);

        $nav->section([
            $nav->title('Finance'),

            $nav->preset(PromotionalOfferConfig::class, ['icon' => fa('offers')]),
            $nav->preset(RentDiscountConfig::class, ['icon' => fa('offers')]),
            $nav->preset(DiscountConfig::class, ['icon' => fa('money-bill')]),
            $nav->preset(PaymentTypeConfig::class, ['icon' => fa('money-bill')]),
        ]);

        $nav->section([
            $nav->title('Bookings'),

            $nav->preset(BookingConfig::class, ['icon' => fa('check')]),
            $nav->preset(TaxiBookingConfig::class, ['icon' => fa('taxi')]),
            $nav->preset(TourBookingConfig::class, ['icon' => fa('map')]),
            $nav->preset(FeedbackConfig::class, ['icon' => fa('comment-dots')])
        ]);
    }
}
