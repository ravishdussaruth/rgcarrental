<?php

namespace Lit\Config\Crud;

use App\Models\Discount;
use Ignite\Crud\Config\CrudConfig;
use Ignite\Crud\CrudIndex;
use Ignite\Crud\CrudShow;
use Lit\Http\Controllers\Crud\DiscountController;

class DiscountConfig extends CrudConfig
{
    /**
     * Model class.
     *
     * @var string
     */
    public $model = Discount::class;

    /**
     * Controller class.
     *
     * @var string
     */
    public $controller = DiscountController::class;

    /**
     * Model singular and plural name.
     *
     * @param Discount|null discount
     *
     * @return array
     */
    public function names(Discount $discount = null)
    {
        return [
            'singular' => 'Discount',
            'plural'   => 'Discounts',
        ];
    }

    /**
     * Get crud route prefix.
     *
     * @return string
     */
    public function routePrefix()
    {
        return 'discounts';
    }

    /**
     * Build index page.
     *
     * @param \Ignite\Crud\CrudIndex $page
     *
     * @return void
     */
    public function index(CrudIndex $page)
    {
        $page->table(function ($table) {
            $table->col('Name')->value('{name}');
            $table->col('Coefficient')->value('{coeff}');
        })->search('title');
    }

    /**
     * Setup show page.
     *
     * @param \Ignite\Crud\CrudShow $page
     *
     * @return void
     */
    public function show(CrudShow $page)
    {
        $page->card(function ($form) {
            $form->input('name')
                 ->title('Discount Title');

            $form->input('coeff')
                 ->title('Discount Coefficient')
                 ->hint('Ex: 25% will be 0.25 (25/100)');
        });
    }
}
