import axios from 'axios';

/**
 * Create a booking for that user.
 *
 * @param {Object} data
 *
 * @return {Promise<AxiosResponse<any>>}
 */
export const book = (data) => axios.post('/book', data);

/**
 * Check the price and return discount if available.
 *
 * @param {Object} data
 *
 * @return {Promise<AxiosResponse<any>>}
 */
export const checkPrice = (data) => axios.post('/check-price', data);

/**
 * Check the coupon and return discount if available.
 *
 * @param {Object} data
 *
 * @return {Promise<AxiosResponse<any>>}
 */
export const verifyCoupon = (data) => axios.post('/validate-coupon', data);