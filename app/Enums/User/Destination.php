<?php

namespace App\Enums\User;

use App\Enums\Enum;

class Destination extends Enum
{
    /**
     * All destinations belonging to north.
     *
     * @var int
     */
    const NORTH = 1;

    /**
     * All destinations belonging to the east.
     *
     * @var int
     */
    const EAST = 2;

    /**
     * All destinations belonging to the west.
     *
     * @var int
     */
    const WEST = 3;

    /**
     * All destinations belonging to the south.
     *
     * @var int
     */
    const SOUTH = 4;
}
