<?php

namespace App\Notifications\Booking\Taxi;

use App\Notifications\Booking\Concerns\BookingNotification;

class NotifyUserBookingConfirmed extends BookingNotification
{
    /**
     * The view path.
     *
     * @return string
     */
    protected function view(): string
    {
        return 'emails.booking.taxi.confirmed';
    }

    /**
     * The email subject.
     *
     * @return string
     */
    protected function subject(): string
    {
        return 'Taxi Booking Confirmed';
    }
}
