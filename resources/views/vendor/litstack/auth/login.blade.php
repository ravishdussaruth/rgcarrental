@extends('litstack::landing')

@section('title')
    Login
@endsection

@section('content')
    <div class="d-flex flex-lg-row flex-column vh-100" id="login">
        <div class="col-12 col-lg-6 h-100">
            <div class="d-flex justify-content-center align-items-center h-100">
                <div class="col-lg-8">
                    <form method="POST" id="login" onsubmit="doLogin(event)" class="mt-4 mb-4">
                        @csrf
                        <div class="form-fields">
                            <div class="form-group mb-5">
                                <input
                                    id="email"
                                    class="form-control @error('email') is-invalid @enderror lit-login-form"
                                    name="email"
                                    required
                                    @if(config('lit.login.username'))
                                    placeholder="{{ ucfirst(__lit('login.email_or_username')) }} "
                                    type="text"
                                    @else
                                    placeholder="{{ ucfirst(__lit('base.email')) }}"
                                    autocomplete="email"
                                    type="email"
                                    @endif
                                    autofocus
                                />

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group mb-3">
                                <input
                                    placeholder="{{ ucfirst(__lit('base.password')) }}"
                                    id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror lit-login-form"
                                    name="password"
                                    required
                                    autocomplete="current-password"
                                />

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror

                                @if(Session::has('status'))
                                    <span class="valid-feedback"
                                          style="display:block;">{{ Session::get('status') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group d-flex justify-content-between">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember">

                                <label class="form-check-label" for="remember">
                                    {{ __lit('login.remember_me') }}
                                </label>
                            </div>
                            @if(Route::has('lit.password.forgot.show'))
                                <div>
                                    <a href="{{ lit()->route('password.forgot.show') }}">{{ __lit('login.forgot_password') }}</a>
                                </div>
                            @endif
                        </div>

                        <div class="text-danger text-center" id="login-failed" style="display:none;">
                            {{ __lit('login.failed') }}
                        </div>

                        <div class="form-group row mt-4 justify-content-center d-flex">
                            <button type="submit" class="btn btn-primary">
                                {{ __lit('login.do_login') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-lg-6 order-lg-last order-first text-center dark-logo"
             style="background:#0a0e23; display: flex; justify-content: center; align-items: center;">
            @include('litstack::partials.logo')
        </div>
    </div>
    <script type="text/javascript">
        const loginRoute = '{{ Lit::route('login.post') }}';
        window.doLogin = function (e) {
            e.preventDefault()

            const data = new FormData(document.forms.login);

            let promise = axios.post(loginRoute, data)
            promise.then(function (response) {
                window.location = response.data
            })
            promise.catch(function (error) {
                document.getElementById('login-failed').style.display = 'block';
                document.getElementById('forgot-password').style.display = 'block';
            })
        }
    </script>
    @if(isset($script))
        <script src="{{ $script }}"></script>
    @endif
    @foreach(lit_app()->getLoginScripts() as $src)
        <script src="{{ $src }}"></script>
    @endforeach
@endsection
