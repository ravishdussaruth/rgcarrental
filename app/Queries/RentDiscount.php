<?php

namespace App\Queries;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class RentDiscount
{
    /**
     * Build the query.
     *
     * @param  int  $numberOfDays
     * @return Builder
     */
    public static function execute(int $numberOfDays): Builder
    {
        return \App\Models\RentDiscount::query()
                                       ->whereRaw(
                                           DB::raw($numberOfDays . ' BETWEEN `min_day` AND `max_day` ')
                                       );
    }
}