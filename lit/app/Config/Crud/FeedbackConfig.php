<?php

namespace Lit\Config\Crud;

use App\Models\Booking;
use App\Models\Feedback;
use Ignite\Crud\Config\CrudConfig;
use Ignite\Crud\CrudIndex;
use Ignite\Crud\CrudShow;
use Lit\Http\Controllers\Crud\FeedbackController;

class FeedbackConfig extends CrudConfig
{
    /**
     * Model class.
     *
     * @var string
     */
    public $model = Feedback::class;

    /**
     * Controller class.
     *
     * @var string
     */
    public $controller = FeedbackController::class;

    /**
     * Model singular and plural name.
     *
     * @param Feedback|null feedback
     *
     * @return array
     */
    public function names(Feedback $feedback = null)
    {
        return [
            'singular' => 'Feedback',
            'plural'   => 'Feedbacks',
        ];
    }

    /**
     * Get crud route prefix.
     *
     * @return string
     */
    public function routePrefix()
    {
        return 'feedbacks';
    }

    /**
     * Build index page.
     *
     * @param \Ignite\Crud\CrudIndex $page
     *
     * @return void
     */
    public function index(CrudIndex $page)
    {
        $page->table(function ($table) {
            $table->col('Text')->value('feedback');
            $table->col('Ratings')->value('Ratings');
            $table->col('On Website')->value('display_on_website', [
                true  => '<div class="badge badge-success">Yes</div>',
                false => '<div class="badge badge-danger">No</div>',
            ]);
        });
    }

    /**
     * Setup show page.
     *
     * @param \Ignite\Crud\CrudShow $page
     *
     * @return void
     */
    public function show(CrudShow $page)
    {
        $page->card(function ($form) {
            $form->select('booking_id')
                 ->title('Booking')
                 ->options(
                     Booking::all()->mapWithKeys(fn(Booking $booking) => [$booking->id => $booking->reference])->toArray()
                 )
                 ->hint('Select a Booking.');

            $form->title('Text')
                 ->textarea('feedback');

            $form->title('Rating')
                 ->input('ratings')
                 ->type('number');

            $form->boolean('display_on_website')
                 ->title('On Website ?');
        });
    }
}
