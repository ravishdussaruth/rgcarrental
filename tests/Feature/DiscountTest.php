<?php

namespace Tests\Feature;

use App\Models\Discount;
use App\Models\RentDiscount;
use Tests\TestCase;

class DiscountTest extends TestCase
{
    /**
     * Test that a rent discount is applied.
     *
     * @return void
     */
    public function testRentDiscountIsApplied()
    {
        // Given a discount.
        [$discount25, $discount50] = Discount::factory()->createMany([
            [
                'name' => '25',
                'coeff' => 0.25,
            ],
            [
                'name' => '50',
                'coeff' => 0.5,
            ],
        ]);

        RentDiscount::factory()->createMany([
            [
                'min_day' => 1,
                'max_day' => 2,
                'discount_id' => $discount25->id,
            ],
            [
                'min_day' => 3,
                'max_day' => 6,
                'discount_id' => $discount50->id,
            ],
        ]);

        $daysBooked = 9;

        $discountToApply = \App\Queries\RentDiscount::execute($daysBooked)->first();

        $this->assertEquals($discount25->id, $discountToApply->id);
    }
}
