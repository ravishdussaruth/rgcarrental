<?php

namespace Lit\Config\Form\Pages;

use Lit\Models\User;
use Ignite\Crud\CrudShow;
use Ignite\Crud\Config\FormConfig;
use Lit\Http\Controllers\Form\Pages\HomeController;

class HomeConfig extends FormConfig
{
    /**
     * Controller class.
     *
     * @var string
     */
    public $controller = HomeController::class;

    /**
     * Form route prefix.
     *
     * @return string
     */
    public function routePrefix()
    {
        return "pages/home";
    }

    /**
     * Form singular name. This name will be displayed in the navigation.
     *
     * @return array
     */
    public function names()
    {
        return [
            'singular' => 'Home',
        ];
    }

    /**
     * Setup form page.
     *
     * @param \Lit\Crud\CrudShow $page
     *
     * @return void
     */
    public function show(CrudShow $page)
    {
        $page->card(function ($form) {
            $form->image('cover')
                 ->title('Cover')
                 ->maxFiles(1);

            $form->image('logo')
                 ->title('Logo')
                 ->maxFiles(1);

            $form->input('slogan')
                 ->title('Website Slogan');
        });

        $page->card(function ($form) {
            $form->textarea('meta')
                 ->title('SEO Meta')
                 ->placeholder('Type something...')
                 ->hint('Your website seo meta config, add comma after each word.');
        });

        $page->card(function ($form) {
            $form->select('contact_user')
                 ->width(1 / 2)
                 ->title('Contact User')
                 ->options(
                     User::all()
                         ->mapWithKeys(fn(User $user) => [
                             $user->id => $user->username
                         ])->toArray()
                 );

            $form->block('contact_numbers')
                 ->title('Contacts')
                 ->repeatables(function ($repeatables) {
                     $repeatables->add('contact', function ($form, $preview) {
                         // The block preview.
                         $preview->col('{contact_number}');

                         // Containing as many form fields as you want.
                         $form->input('contact_number')
                              ->title('Contact');
                     });
                 });

            $form->input('address')
                 ->title('Address');

            $form->input('email_address')
                 ->title('Email Address');
        });
    }
}
