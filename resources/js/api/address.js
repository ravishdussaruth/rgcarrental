import axios from 'axios';

/**
 * Search for an address.
 *
 * @param query
 *
 * @return {Promise<AxiosResponse<any>>}
 */
export const search = (query) => axios.get('/address', {params: {query}});
