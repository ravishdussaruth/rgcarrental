<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@php
	$home = resolve(\App\Contracts\Home::class);
@endphp
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="{{ mix('css/app.css') }}"/>
	<title>@yield('title')</title>

	{!! Robots::metaTag() !!}

	@yield('header')
</head>

<body>
<div id="app">
	<navbar class="mb-5" logo-src="/images/logo.png"
			:is-on-home-page="{{ (int) request()->is('/') }}">
	</navbar>

	@yield('app')

	<footer style="{{ $position ?? '' }};bottom: 0;background: #f9f9f9;padding: 30px;">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="single-footer-item">
						<div class="footer-logo">
							<img src="/images/logo.png" alt="RgCarRental" height="auto" width="200px">
						</div>
					</div>
				</div>

				<div class="col-sm-3">
					<div class="single-footer-item">
						<h6 class="text-left text-capitalize font-weight-bolder text-display">link</h6>

						<div class="single-footer-txt">
							<p><a href="{{ route('home') }}">Home</a></p>
							<p><a href="{{ route('home') . '#services' }}">Services</a></p>
							<p><a href="{{ route('home') . '#destinations' }}">Destinations</a></p>
							<p><a href="{{ route('home') . '#cars' }}">Cars</a></p>
						</div>
					</div>
				</div>

				<div class="col-sm-3">
					<div class="single-footer-item">
						<h6 class="text-left text-capitalize font-weight-bolder text-display">Popular Destination
						</h6>

						<div class="single-footer-txt">
							@foreach (\App\Enums\User\Destination::all() as $destination)
								<p>
									<a
											href="{{ route('app.destinations', ['category' => $destination['label']]) }}">{{ $destination['label'] }}</a>
								</p>
							@endforeach
						</div>
					</div>
				</div>

				<div class="col-sm-3">
					<div class="single-footer-item text-center">
						<h6 class="text-left text-capitalize font-weight-bolder text-display">Contacts</h6>
						<div class="single-footer-txt text-left">
							@foreach($home->contactNumbers() as $contact)
								<p>{{ $contact }}</p>
							@endforeach
							<span class="text-display foot-email text-lowercase"><a
										href="mailto:{{ $home->email() }}">{{ $home->email() }}</span>
							<p>{{ $home->address() }}</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
</div>

<script defer src="{{ mix('js/manifest.js') }}"></script>
<script defer src="{{ mix('js/vendor.js') }}"></script>
<script defer src="{{ mix('js/app.js') }}"></script>
</body>

</html>
