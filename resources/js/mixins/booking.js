import {map, isEmpty} from 'lodash';

export default {
    data: () => ({
        isCreating: false
    }),

    methods: {
        /**
         * The params for the booking.
         *
         * @return {Object}
         */
        params() {
            throw Error('"params" method is not implemented');
        },

        /**
         * Will tell us if the booking is validated.
         *
         * @return {Boolean}
         */
        isUserValidated() {
            return false;
        },

        /**
         * The create method, in which the api will be contained.
         *
         * @param {Object} params
         *
         * @return {Promise}
         */
        async create(params) {
            throw Error('"create" method is not implemented');
        },

        /**
         * Start the creation process.
         */
        async startCreation() {
            try {
                this.isCreating = true;
                let {data: {booked, reference}} = await this.create(this.params());
                this.isCreating = false;

                if (booked) {
                    this.$swal.fire({
                        icon: 'success',
                        title: 'Yaayyyy...',
                        text: `Your booking has been placed. Please copy this ref number: ${reference}`
                    }).then((() => {
                        window.open('/')
                    }));

                    return;
                }

                this.$swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: `Sorry, your booking could not be placed.`
                });
            } catch ({response: {data: {errors}}}) {
                this.isCreating = false;
                console.log('here', errors, map(errors, (value) => value).join(', '))

                if (!isEmpty(errors)) {
                    this.$swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: map(errors, (value) => value[0]).join(', ')
                    });
                }
            }
        }
    }
}