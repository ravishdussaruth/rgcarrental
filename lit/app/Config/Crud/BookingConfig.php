<?php

namespace Lit\Config\Crud;

use App\Models\Car;
use App\Models\User;
use App\Models\Booking;
use App\Models\Location;
use Ignite\Crud\CrudIndex;
use Ignite\Crud\CrudIndexTable;
use Ignite\Crud\CrudShow;
use App\Enums\BookingType;
use Lit\Config\Crud\Concerns\BaseBookingConfig;
use Lit\Http\Controllers\Crud\RentalBookingController;

class BookingConfig extends BaseBookingConfig
{
    /**
     * Controller class.
     *
     * @var string
     */
    public $controller = RentalBookingController::class;

    /**
     * Model singular and plural name.
     *
     * @param Booking|null booking
     *
     * @return array
     */
    public function names(Booking $booking = null)
    {
        return [
            'singular' => $booking->reference ?? 'Rental',
            'plural'   => 'Rentals',
        ];
    }

    /**
     * Get crud route prefix.
     *
     * @return string
     */
    public function routePrefix()
    {
        return 'bookings';
    }

    /**
     * Build index page.
     *
     * @param \Ignite\Crud\CrudIndex $page
     *
     * @return CrudIndexTable
     */
    protected function table(CrudIndex $page): CrudIndexTable
    {
        return $page->table(function ($table) {
            $table->col('Reference')
                  ->value('reference');

            $table->col('User')
                  ->value('{user.first_name} {user.last_name}');

            $table->col('Pickup Location')
                  ->value('pickup_location.location_name');

            $table->col('Drop Location')
                  ->value('drop_location.location_name');

            $table->col('Pickup time')
                  ->value('pickup_time');

            $table->col('Approved')->value('approved', [
                true  => '<div class="badge badge-success">Approved</div>',
                false => '<div class="badge badge-danger">Waiting for approval</div>',
            ])->sortBy('approved');
        })->query(function ($query) {
            $query->select(['id', 'reference', 'user_id', 'pickup_time', 'pickup_id', 'drop_id', 'approved'])
                  ->with(['user:id,first_name,last_name', 'dropLocation:id,location_name', 'pickupLocation:id,location_name'])
                  ->where('booking_type', BookingType::RENT);
        });
    }

    /**
     * The booking form.
     *
     * @param CrudShow $page
     *
     * @return void
     */
    protected function form(CrudShow $page): void
    {
        $page->select('car_id')
             ->title('Car')
             ->options(
                 Car::all()->mapWithKeys(fn($item) => [$item->id => $item->name])->toArray()
             )->hint('Select a car')
             ->width(1 / 2);

        $page->select('pickup_id')
             ->title('Pickup Location')
             ->options(
                 Location::all()->mapWithKeys(fn($item) => [$item->id => $item->location_name])->toArray()
             )->hint('Select a pickup location')
             ->width(1 / 2);

        $page->select('drop_id')
             ->title('Drop Location')
             ->options(
                 Location::all()->mapWithKeys(fn($item) => [$item->id => $item->location_name])->toArray()
             )->hint('Select a drop location')
             ->width(1 / 2);

        $page->datetime('pickup_time')
             ->title('Pickup Time')
             ->onlyDate(false)
             ->formatted('l')
             ->hint('Choose Pickup Time')
             ->width(1 / 2);

        $page->datetime('drop_time')
             ->title('Drop Time')
             ->onlyDate(false)
             ->formatted('l')
             ->hint('Choose Drop Time')
             ->width(1 / 2);

        $page->datetime('delivered_on')
             ->title('Delivered Time')
             ->onlyDate(false)
             ->formatted('l')
             ->hint('Choose Delivered Time')
             ->width(1 / 2);

        $page->datetime('returned_on')
             ->title('Returned on')
             ->onlyDate(false)
             ->formatted('l')
             ->hint('Choose Returned Time')
             ->width(1 / 2);

        $page->onlyOnUpdate(function ($form) {
            $form->textarea('rejected_comment')
                 ->title('Add comment if booking is being rejected');
        });
    }
}
