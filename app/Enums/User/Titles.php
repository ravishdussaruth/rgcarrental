<?php

namespace App\Enums\User;

class Titles extends \App\Enums\Enum
{
    /**
     * @var int
     */
    const MR = 1;

    /**
     * @var int
     */
    const MRS = 2;

    /**
     * @var int
     */
    const MISS = 3;
}
