<?php

namespace App\Providers;

use App\Contracts\Home;
use App\Services\Website;
use App\Contracts\Location as Query;
use App\Services\GeoLocation;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Home::class, Website::class);
        $this->app->bind(Query::class, GeoLocation::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
