<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;
use App\Contracts\Location as Query;

class GeoLocation implements Query
{
    /**
     * The request parameters.
     *
     * @var array
     */
    protected $params = [];

    /**
     * Use this parameter.
     *
     * @param string $param
     * @param        $value
     *
     * @return self
     */
    public function withParam(string $param, $value): Query
    {
        $this->params[$param] = $value;

        return $this;
    }

    /**
     * All the params list.
     *
     * @return array
     */
    public function parameters(): array
    {
        return array_merge([
            'key' => service('locationiq.key'),
            'countrycodes' => service('locationiq.country')
        ], $this->params);
    }

    /**
     * Run the request.
     *
     * @return mixed
     */
    public function get()
    {
        return Http::get(service('locationiq.url'), $this->parameters());
    }

    /**
     * Set the parameters.
     *
     * @param $name
     * @param $value
     *
     * @return self
     */
    public function __call($name, $value)
    {
        method_exists($this, $name) ? $this->{$name}($value) : $this->withParam($name, $value[0]);

        return $this;
    }
}
